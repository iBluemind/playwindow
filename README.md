# PlayWindow #

'SKT API를 활용한 스마트 서비스 개발 공모전'(https://developers.sktelecom.com/story/contest/)에 출품하기 위한 작품.
SKT API인 BaaS와 PlayRTC를 이용함.

## 만든사람 ##

* 김재욱(Kim Jaewook)
* 한만종(Han Manjong)

## 사용된 라이브러리 ##

* Android Support Library v7 AppCompat revision 21 (https://developer.android.com/intl/ko/tools/support-library/setup.html)
* gcm-client-deprecated (https://code.google.com/p/gcm/)
* PagerSlidingTabStrip (https://github.com/jpardogo/PagerSlidingTabStrip)
* Facebook SDK (https://developers.facebook.com/docs/android/getting-started)
* Retrofit, Gson, PlayRTC, SKT BaaS, ListViewAnimations, Android Support Library v4

## 라이센스 ##

Copyright 2014 Wookjong Team(Kim Jaewook, Han Manjong)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
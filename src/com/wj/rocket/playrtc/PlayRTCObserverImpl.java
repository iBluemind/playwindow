package com.wj.rocket.playrtc;

import android.content.SharedPreferences;
import android.util.Log;

import com.skt.baas.api.BaasInstallation;
import com.sktelecom.playrtc.PlayRTC;
import com.sktelecom.playrtc.PlayRTC.PlayRTCCode;
import com.sktelecom.playrtc.PlayRTC.PlayRTCStatus;
import com.sktelecom.playrtc.observer.PlayRTCObserver;
import com.sktelecom.playrtc.stream.PlayRTCMedia;
import com.wj.rocket.activity.CameraActivity;
import com.wj.rocket.util.AppUtil;
import com.wj.rocket.util.Constants;
import com.wj.rocket.util.LogUtil;

public class PlayRTCObserverImpl extends PlayRTCObserver implements Constants {
	
	private CameraActivity activity = null;
	private VideoViewHandler viewHandler = null;
	
	public PlayRTCObserverImpl(CameraActivity activity, VideoViewHandler viewHandler) {
		this.activity = activity;
		this.viewHandler = viewHandler;
		
	}

	/**
	 * PlayRTC 플랫폼 채널 서비스에 채널을 생성 한후 채널 아이디를 전달 위한 인터페이스
	 * @param obj PlayRTC
	 * @param channelId String, 생성된 채널 아이디
	 * @param reson String, 채널 접속 이유 "create", "connect"
	 */
	@Override
	public void onConnectChannel(final PlayRTC obj, final String channelId, final String reason) {
		if(reason.equals("create")) {
			activity.saveChannelId(channelId);
		}
	}
	
	/**
	 * PlayRTC 객체가 로컬 미디어 스트림객체를 얻어 PlayRTCMedia를 생성하고 전달하기 위해 호출하는 인터페이스<br>
	 * PlayRTCVIdeView에 영싱 출력을 해야한다.
	 * @param obj PlayRTC
	 * @param media PlayRTCMedia
	 */
	@Override
	public void onAddLocalStream(final PlayRTC obj, final PlayRTCMedia rtcMedia) {
		if (activity.callingType == CALL_TYPE_REQUEST) {
			viewHandler.hideCoverView(500);
			viewHandler.renderMediaStream(rtcMedia);
		}
	}

	/**
	 * PlayRTC 객체가 P2P가 연결되어 상대방 미디어 스트림객체를 얻어 PlayRTCMedia를 생성하고 전달하기 위해 호출하는 인터페이스<br>
	 * PlayRTCVIdeView에 영싱 출력을 해야한다.
	 * @param obj PlayRTC
	 * @param peerId String, 사용자의 채널 아이디 
	 * @param peerUid String, 사용자의 application에서 사용하는 아이디로 채널 연결 시 입력한 값, 없으면 ""
	 * @param media PlayRTCMedia
	 */
	@Override
	public void onAddRemoteStream(final PlayRTC obj, final String peerId, final String peerUid, final PlayRTCMedia rtcMedia) {
		if (activity.callingType == CALL_TYPE_RESPONSE) {
			// 미디어 객체에 PlayRTCVideoView를 전달하여 화면 출력
			viewHandler.renderMediaStream(rtcMedia);
		}
	}

	/**
	 * 자신이 채널에서 퇴장할 때 호출하는 인터페이스 <br>
	 * NooNooN Application에서는 통신 종료 시 deleteChannel을 사용하므로 정상 적인 상황이라면<br>
	 * 모든 사용자는 onDisconnectChannel:"close"를 받게 된다.<br>
	 * @param obj PlayRTC
	 * @param reason String, 채널 퇴장이면 "disconnect" 채널 종료이면 "delete"
	 */
	@Override
	public void onDisconnectChannel(final PlayRTC obj, final String reason) {
		Log.d("PLAYRTC", "onDisconnectChannel....");
		
		// 마스터 볼륨값을 원래 값으로 되돌린다.
		activity.restoreMasterVolume();
		viewHandler.showCoverView();
		activity.removeRegistStatus(PLAYRTC_STAT_CONNECTED);
		
		// 내가 back키를 눌러 채널을 퇴장하는 경우
		if(activity.checkRegistStatus(PLAYRTC_STAT_DISCONNECTING) == true) {
			LogUtil.d("PLAYRTC", "onCloseChannel :: 내가 채널을 종료하여 이벤트를 수신한 경우....");
			activity.finish();
		}
		// 상대방이 채널을 종료한 경우 
		else {
			activity.setRegistStatus(PLAYRTC_STAT_DISCONNECTING);
			if (activity.callingType == CALL_TYPE_REQUEST) {
				activity.resetPlayRTCAndReady();
			} else {
				activity.finish();
			}
		}
	}
	
	
	/**
	 * 상대방이 채널에서 퇴장할 때 호출하는 인터페이스 <br>
	 * NooNooN Application에서는 통신 종료 시 deleteChannel을 사용하므로 정상 적인 상황이라면<br>
	 * 모든 사용자는 onDisconnectChannel:"close"를 받게 된다.
	 * @param obj PlayRTC
	 * @param peerId String, 사용자의 채널 아이디 
	 * @param peerUid String, 사용자의 application에서 사용하는 아이디로 채널 연결 시 입력한 값, 없으면 ""
	 */
	@Override
	public void onOtherDisconnectChannel(final PlayRTC obj, final String peerId, final String peerUid) {
		AppUtil.showToast(activity, "P2P["+peerUid+"] Closed....");
		
		activity.restoreMasterVolume();
		viewHandler.showCoverView();
		activity.removeRegistStatus(PLAYRTC_STAT_CONNECTED);
		
		LogUtil.d("PLAYRTC", "onOtherDisconnectChannel :: 상대방이 채널을 퇴장하여 이벤트를 수신한 경우....");
		
		if (activity.callingType == CALL_TYPE_REQUEST) {
			activity.resetPlayRTCAndReady();
		} else {
			activity.finish();
		}
		
	}
	
	/**
	 * PlayRTC 객체의 오류 정보를 전달하기 위한 인터페이스 
	 * 
	 * @param obj PlayRTC
	 * @param status PlayRTCStatus PlayRTC 객체의 상태 정보
	 * @param code PlayRTCCode PlayRTC 객체의 오류 코드 정보 
	 * @param desc String Description 
	 */
	@Override
	public void onError(PlayRTC obj, PlayRTCStatus status, PlayRTCCode code, String desc) {
		// 오류 상태값 등록 
		activity.setRegistStatus(PLAYRTC_STAT_ERROR);
		AppUtil.showToast(activity, "Error["+ code + "] Status["+ status+ "] "+desc);

		if (activity.callingType == CALL_TYPE_REQUEST) {
			activity.removeRegistStatus(PLAYRTC_STAT_CONNECTED);
			activity.setRegistStatus(PLAYRTC_STAT_DISCONNECTING);
		}
		activity.finish();		
	}

	/**
	 * PlayRTC 객체의 상태변경 정보를 전달 받기 위한 인터페이스 
	 * 
	 * @param obj PlayRTC
	 * @param peerId String, 사용자의 채널 아이디 
	 * @param peerUid String, 사용자의 application에서 사용하는 아이디로 채널 연결 시 입력한 값, 없으면 ""
	 * @param status PlayRTCStatus PlayRTC 객체의 상태 정보
	 * @param desc String
	 */
	@Override
	public void onStateChange(final PlayRTC obj, final String peerId, final String peerUid, final PlayRTCStatus status, final String desc) {
		LogUtil.d("PLAYRTC", "onStateChange status=" + status);
		// p2p 객체 생성 
		if(status == PlayRTCStatus.PeerCreation) {
			LogUtil.d("PLAYRTC", peerId+" get ready");
		}
		// p2p 연결 시도중 
		else if(status == PlayRTCStatus.PeerConnecting) {
			LogUtil.d("PLAYRTC", peerId+" is joined");
		}
		// P2P 연결 수립 
		else if(status == PlayRTCStatus.PeerConnected) {
			
			viewHandler.hideCoverView(500);
//			
//			// 연결 대기 시간 체크 타이머 정지 
//			activity.stopCountDownTimer();
//			// 통화 시간 체크 타이머 시작 
//			activity.startRunTimer();
//			// 연결 시작 시간 등록 
//			activity.setConnectedTime(System.currentTimeMillis());
//			activity.getUsageData().setConnectTime(AppUtil.getDateTime(new Date(), "MM.dd yyyy HH:mm"));
//			
//			// 사용기록을 위해 상태 값을 지정한다. 
//			if(activity.isCallingTypeRequest()) {
//				activity.getUsageData().setStatus(UsageData.STATUS_CALL_DONE);
//			}
//			else {
//				activity.getUsageData().setStatus(UsageData.STATUS_CALLEE_DONE);
//			}
//			
//			// 타이틀바 문구 변경 
//			titleBar.setStatusText(activity.getString(R.string.playrtc_msg_establish));
//			// 시스템 키볼륨 조정 
//			app.setSystemVolume(1);
//			// 음성 통신을 위해 볼륨 설정 
			AppUtil.setMasterVolume(activity, PLAYRTC_VOICE_VOLUME);
//			AppUtil.showToast(activity, uid+" "+ activity.getString(R.string.playrtc_msg_establish));
		}
		// P2P 연결 종료 
		else if(status == PlayRTCStatus.PeerDisconnected) {
//			AppUtil.showToast(activity, uid+" "+ activity.getString(R.string.playrtc_msg_closed));
		}
		// 그외 
		else {
			//AppUtil.showToast(PlayRTCActivity.this, peerId+"  Status["+ status+ "]...");
		}

	}
}

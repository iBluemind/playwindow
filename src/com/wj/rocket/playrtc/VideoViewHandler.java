package com.wj.rocket.playrtc;

import android.graphics.Point;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.sktelecom.playrtc.stream.PlayRTCMedia;
import com.sktelecom.playrtc.util.ui.PlayRTCVideoView;
import com.wj.rocket.R;
import com.wj.rocket.activity.CameraActivity;

public class VideoViewHandler {

	private CameraActivity activity = null;
	// PlayRTCVideoView를 위한 부모 뷰 그룹
	private RelativeLayout videoArea = null;
	private PlayRTCVideoView videoView = null;
	private RelativeLayout videoCoverView = null;
	
	// 로컬 PlayRTCMedia 전역 변수 
	private PlayRTCMedia rtcMedia = null;
	

	public VideoViewHandler(CameraActivity activity) {
		this.activity = activity;
		initLayout();
	}
	
	private void initLayout() {
		//PlayRTCVideoView객체의 부모 뷰 PlayRTCVideoView는 동적으로 생성한다.
		this.videoArea = (RelativeLayout)activity.findViewById(R.id.videoarea);
		// CoverView 크기조절 
		this.videoCoverView = (RelativeLayout)activity.findViewById(R.id.media_coverlayout);
	}
	
	public boolean hasStreamView() {
		return this.videoView != null;
	}
	
	public void createVideoStreamView() {
		/* video 스트림 출력을 위한 PlayRTCVideoView의 부모 ViewGroup의 사이즈 재조정 
		 */
		RelativeLayout layout = (RelativeLayout)activity.findViewById(R.id.media_layout);
		float _width = layout.getWidth();
		float _height = layout.getHeight();
		
		// PlayRTCVideoView의 부모 뷰 크기 조절 
		RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams)videoArea.getLayoutParams();
		param.width = (int)_width;
		param.height = (int)_height;
		videoArea.setLayoutParams(param);

		
		// 영상 출력 메인 뷰 동적 생성, 사용자(도움요청자) : 로컬 영상 출력 , 도우미 상대방 영상 출력 
		createMainStreamVideoView(_width, _height);

		FrameLayout.LayoutParams param1 = (FrameLayout.LayoutParams)videoCoverView.getLayoutParams();
		param1.width = (int)_width;
		param1.height = (int)_height;
		videoCoverView.setLayoutParams(param1);

	}
	
	
	public void showCoverView() {
		videoCoverView.post(new Runnable(){
			public void run() {
				videoCoverView.setVisibility(View.VISIBLE);
			}
		});
	}
	
	public void hideCoverView(long delayed) {
		if(delayed < 0)delayed = 0;
		videoCoverView.postDelayed(new Runnable(){
			public void run() {
				videoCoverView.setVisibility(View.GONE);
			}
		}, delayed);
	}
	
	
	public PlayRTCVideoView getMainVideoView() {
		return videoView;
	}

	public void renderMediaStream(PlayRTCMedia media) {
		rtcMedia = media;
		rtcMedia.setVideoRenderer(videoView.getVideoRenderer());
	}
	
	
	public void setVideoMute(int mediaType, boolean mute) {
		if(rtcMedia != null) {
			rtcMedia.setVideoMute(mute);
		}
	}
	
	public void setAudioMute(int mediaType, boolean mute) {
		if(rtcMedia != null) {
			rtcMedia.setAudioMute(mute);
		}
	}
	
	// 영상 스트림 출력을 위한 PlayRTCVideoView(GLSurfaceView를 상속) 동적 코드 생성  
	// 영상 출력 메인 뷰 동적 생성, 사용자(도움요청자) : 로컬 영상 출력 , 도우미 상대방 영상 출력 
	private void createMainStreamVideoView(float width, float height) {
		if(videoView != null) return;
		
		Point displaySize = new Point();
		displaySize.x = (int)width;
		displaySize.y = (int)height;
		
		//PlayRTCVideoView(GLSurfaceView를 상속) 동적 생성, 생성자에 화면 사이즈 전달 
		videoView = new PlayRTCVideoView(activity.getApplicationContext(), displaySize);
		RelativeLayout.LayoutParams viewParam = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		videoView.setLayoutParams(viewParam);

		// 부모뷰에 추가 
		videoArea.addView(videoView);
	}
	
}

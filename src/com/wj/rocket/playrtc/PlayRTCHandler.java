package com.wj.rocket.playrtc;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Environment;

import com.skt.baas.api.BaasObject;
import com.skt.baas.callback.BaasDeleteCallback;
import com.skt.baas.exception.BaasException;
import com.sktelecom.playrtc.PlayRTC;
import com.sktelecom.playrtc.PlayRTCFactory;
import com.sktelecom.playrtc.config.PlayRTCSettings;
import com.sktelecom.playrtc.exception.RequiredConfigMissingException;
import com.sktelecom.playrtc.exception.RequiredParameterMissingException;
import com.sktelecom.playrtc.exception.UnsupportedPlatformVersionException;
import com.sktelecom.playrtc.observer.PlayRTCObserver;
import com.wj.rocket.util.AppUtil;

/**
 * PlayRTC의 기능을 제공하기 위한 PlayRTC Wrapper Class.<br>
 *
 */
public class PlayRTCHandler {

	private Activity activity = null;
	private PlayRTC playRTC = null;
	
	public PlayRTCHandler(Activity activity, PlayRTCObserver oberver) 
				throws UnsupportedPlatformVersionException, RequiredParameterMissingException {
	
		this.activity = activity;
		playRTC = PlayRTCFactory.newInstance(oberver);
	}

	public PlayRTCHandler(Activity activity, String serviceUrl, PlayRTCObserver oberver) 
					throws UnsupportedPlatformVersionException, RequiredParameterMissingException {

		this.activity = activity;
		playRTC = PlayRTCFactory.newInstance(serviceUrl, oberver);
	}
	
	public void setConfiguration(String camera, boolean videoEnable) {
		PlayRTCSettings settings = playRTC.getSettings();		
		
		settings.android.setContext(activity.getApplicationContext());
		
		/* 사용할 카메라를 지정  "front", "back" 카메라 지정 */
		settings.setCamera(camera);
		 

		// 영상 스트림 전송 여부를 지정 
		settings.setVideoEnable(videoEnable); 
		
		// 음성 스트림 전송 여부를 지정 
		settings.setAudioEnable(true);   /* 음성 전송 사용 */
		// 데이터 스트림 통신을 사용할 지 여부 
		settings.setDataEnable(false);    /* P2P 데이터 교환을 위한 DataChannel 사용 여부 */
		
		/*  ring, 연결 수립 여부를 상대방에게 묻는지 여부를 지정, true면 상대의 수락이 있어야 연결 수립 진행  */ 
		settings.channel.setRing(false); 
		
//		/* SDK Console 로그 레벨 지정 */
//		settings.log.console.setLevel(PlayRTCSettings.DEBUG);
//		
//		/* SDK 파일 로깅을 위한 로그 파일 경로, 파일 로깅을 사용하지 않는다면 Pass */ 
//        File logPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
//                    "/Android/data/" + activity.getPackageName() + "/files/log");  
//        
        /* SDK 서버 로깅을 위한 로그 임시 캐시 경로, 전송 실패 시 이 경로에 파일 저장을 한다.  */
        File cachePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Android/data/" + activity.getPackageName() + "/files/cache");  
//        
//        
//		/* 파일 로그를 남기려면 로그파일 폴더 지정 . [PATH]/yyyyMMdd.log , 10일간 보존 */
//		settings.log.file.setLogPath(logPath.getAbsolutePath());
//		/* SDK 파일 로그 레벨 지정 */
//		settings.log.file.setLevel(PlayRTCSettings.DEBUG);
		
		/* 서버 로그 전송 실패 시 재 전송 지연 시간, msec  */
		settings.log.setCachePath(cachePath.getAbsolutePath());
		
		settings.log.setRetryQueueDelays(5 * 1000);
		/* 서버 로그 재 전송 실패시 로그 DB 저장 후 재전송 시도 지연 시간, msec */
		settings.log.setRetryCacheDelays(20 * 1000);
	}
	
	/**
	 * 통신을 시작하기 위해 PlayRTC 채널 서비스에 채널을 생성하고 입장힌다.<br>
	 * 채널에 입장하면 채널 서비스는 PlayRTC 서비스 관련 Configuration 정보와 채널 아이디를 빈환하고,<br>
	 * SDK가 정보를 수신하여 냐부적으로 서비스 설정을 한다.<br>
	 * 이과정에서 획득한 채널 아이디를 PlayRTCObserver 인터페이스(onConnectChannel : reson -> "create")를 통해 전달한다.<br>
	 * NooNooN은 이 채널 아이디를 Push 메세지로 상대방에게 전송한다.  <br>
	 * 
	 * @param uid String, NooNooN 사용자 아이디 
	 * @throws RequiredConfigMissingException
	 */
	public void createCommunicationChannel(String uid) throws RequiredConfigMissingException{
		JSONObject parameters = new JSONObject();
		// 채널정보를 정의한다.
		JSONObject channel = new JSONObject();
		try {
			// 채널에 대한 이름을 지정한다.
			channel.put("channelName", AppUtil.makeRequestPlayRTCServiceId(uid));
			parameters.put("channel", channel);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		JSONObject peer = new JSONObject();
		try {
			peer.put("uid", uid);
			parameters.put("peer", peer);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		playRTC.createChannel(parameters);
		
	}
	
	/**
	 * 상대방의 Push 메세지를 받아 전달 받은 채널아이디를 가지고 지생성되어 있는 채널에 입장한다. 
	 * 
	 * @param channelId String, 상대방으로부터 전달 받은 채널의 아이디 
	 * @param uid String, NooNooN 사용자 아이디 
	 * @throws RequiredConfigMissingException
	 */
	public void connectCommunicationChannel(String channelId, String uid) throws RequiredConfigMissingException {
		JSONObject parameters = new JSONObject();

		JSONObject peer = new JSONObject();
		try {
			peer.put("uid", uid);
			parameters.put("peer", peer);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		playRTC.connectChannel(channelId, parameters);
	}
	
	/**
	 * 입장한 채널에서 퇴장한다.<br>
	 * PlayRTCObserver 인터페이스의 onDisconnectChannel : reson-> "disconnect"가 호출되고,<br> 
	 * 상대방은 onOtherDisconnectChannel가 호출된다.
	 * @param peerId String, 채널에서 부여 받은 채널 내부의 사용자 식별아이디 
	 */
	public void disconnectCommunicationChannel() {
		if(playRTC != null){
			playRTC.disconnectChannel(playRTC.getPeerId());
		}

	}
	/**
	 * 입장한 채널을 종료시킨다.<br>
	 * 모든 시용자에게 PlayRTCObserver 인터페이스의 onDeleteChannel : reson-> "close"가 호출된다.
	 */
	public void deleteCommunicationChannel() {
		if(playRTC != null){
			playRTC.deleteChannel();
		}
	}
	
	
	/* Activity onResume */
	public void resume()
	{
		if(playRTC != null){
			playRTC.resume();
		}
	}
	
	
	/* Activity onPause */
	public void pause()
	{if(playRTC != null){
		playRTC.resume();
	}
		
	}
	
	/**
	 * 채널 아이디를 반환한다.
	 * @return String
	 */
	public String getCommunicationChannelId()
	{
		if(playRTC != null){
			return playRTC.getChannelId();
		}
		return null;
	}
	
}

package com.wj.rocket;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.floatingactionbuttonbasic.FloatingActionButton;
import com.google.floatingactionbuttonbasic.FloatingActionButton.OnCheckedChangeListener;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.skt.baas.api.BaasObject;
import com.skt.baas.api.BaasQuery;
import com.skt.baas.api.BaasUser;
import com.skt.baas.callback.BaasDeleteCallback;
import com.skt.baas.callback.BaasListCallback;
import com.skt.baas.exception.BaasException;
import com.wj.rocket.activity.CameraActivity;
import com.wj.rocket.base.BaseActivity;
import com.wj.rocket.base.BaseFragment;
import com.wj.rocket.model.Channel;
import com.wj.rocket.util.Constants;
import com.wj.rocket.util.OnLoadListener;
import com.wj.rocket.util.OnLoginListener;
import com.wj.rocket.util.UserChannelListAdapter;

public class ChannelFragment extends BaseFragment implements OnItemClickListener,
		 OnLoadListener, OnLoginListener, Constants {

	private List<Channel> userChannels;
	private DynamicListView lvUserChannels;
	private UserChannelListAdapter adapter;

	public static ChannelFragment newInstance() {
		ChannelFragment channelFragment = new ChannelFragment();
		channelFragment.setTitle("채널");
		
		return channelFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		setHasOptionsMenu(true);

		View v = inflater.inflate(R.layout.fragment_channel, container, false);
		lvUserChannels = (DynamicListView) v.findViewById(R.id.lvUserChannels);
		TextView tvEmptyUserChannels = (TextView) v
				.findViewById(R.id.tvEmptyUserChannels);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			FloatingActionButton fab2 = (FloatingActionButton) v.findViewById(R.id.fab_2);
	        fab2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(FloatingActionButton fabView, boolean isChecked) {
					switch (fabView.getId()){
		            case R.id.fab_2:
		            	if (!isChecked) {
		            		Builder builder = new AlertDialog.Builder(getActivity());
			    			builder.setTitle(R.string.dialog_create_channel_title);
			    			builder.setMessage(R.string.dialog_create_channel_msg);
			    			final EditText input = new EditText(getActivity());
			    			builder.setView(input);

			    			builder.setPositiveButton(R.string.dialog_create_channel_positive,
			    					new DialogInterface.OnClickListener() {
			    						public void onClick(DialogInterface dialog,
			    								int whichButton) {
			    							Intent intent = new Intent(getActivity(),
			    									CameraActivity.class);
			    							intent.putExtra("callingType", CALL_TYPE_REQUEST);
			    							intent.putExtra("channelName", input.getText()
			    									.toString());
			    							startActivity(intent);
			    						}
			    					});

			    			builder.setNegativeButton(android.R.string.cancel,
			    					new DialogInterface.OnClickListener() {
			    						public void onClick(DialogInterface dialog,
			    								int whichButton) {
			    							dialog.cancel();
			    						}
			    					});

			    			AlertDialog dialog = builder.create();
			    			dialog.getWindow().clearFlags(
			    					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
			    							| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
			    			dialog.getWindow().setSoftInputMode(
			    					WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

			    			dialog.show();
		            	}
		            	
		                break;
		            default:
		                break;
					}
					
				}
			});
		}

		lvUserChannels.setEmptyView(tvEmptyUserChannels);
		lvUserChannels.setOnItemClickListener(this);

		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		getChannelList();
	}

	@Override
	public void lockUI() {
		super.lockUI();
		if (lvUserChannels != null)
			lvUserChannels.setVisibility(View.INVISIBLE);
	}

	@Override
	public void unLockUI() {
		super.unLockUI();
		if (lvUserChannels != null)
			lvUserChannels.setVisibility(View.VISIBLE);
	}

	public void getChannelList() {
		BaasQuery<BaasObject> baasQuery = BaasQuery.makeQuery("Channel");
		baasQuery.whereEqual("userId", BaasUser.getCurrentUser().getObjectId());

		lockUI();
		baasQuery.findInBackground(new BaasListCallback<BaasObject>() {
			@Override
			public void onSuccess(List<BaasObject> results, BaasException e) {
				unLockUI();
				if (e != null) {
					// 에러 처리
				} else {
					if (results != null) {
						if (userChannels == null) {
							userChannels = new ArrayList<Channel>();
						} else {
							userChannels.clear();
						}
						
						for (BaasObject result : results) {
							Channel userChannel = new Channel();
							userChannel.setObjectId(result.getObjectId());
							userChannel.setName(result.getString("name"));
							userChannel.setChannelId(result
									.getString("channelId"));
							// userChannel.set(result.getString("previewUrl"));

							Date createdAt = result.getCreatedAt();
							DateFormat simpleDateFormat = new SimpleDateFormat(
									"yyyy-MM-dd");
							userChannel.setCreatedAt(simpleDateFormat
									.format(createdAt));

							userChannels.add(userChannel);
						}

						if (adapter == null) {
							adapter = new UserChannelListAdapter(
									getActivity(), userChannels);
							lvUserChannels.setAdapter(adapter);
						} else {
							adapter.notifyDataSetChanged();
						}
						
						lvUserChannels.enableSwipeToDismiss(
							    new OnDismissCallback() {
							        @Override
							        public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions) {
							            for (int position : reverseSortedPositions) {
							            	String channelObjectId = userChannels.get(position).getObjectId();
							            	userChannels.remove(position);

							        		BaasObject baasObject = new BaasObject("Channel");
							        		baasObject.setObjectId(channelObjectId);
							        		baasObject.serverDeleteInBackground(new BaasDeleteCallback() {

							        			@Override
							        			public void onSuccess(BaasException e) {
							        				if (e == null) {
							        					((BaseActivity) getActivity()).showToast(R.string.info_complete_delete_channel,
							        							Toast.LENGTH_SHORT, false);
//							        					getChannelList();
							        				}
							        			}
							        		});
							            }
							        }
							    }
							);
					}
				}
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			getChannelList();
			break;
		case R.id.action_create_channel:
			Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(R.string.dialog_create_channel_title);
			builder.setMessage(R.string.dialog_create_channel_msg);
			final EditText input = new EditText(getActivity());
			builder.setView(input);

			builder.setPositiveButton(R.string.dialog_create_channel_positive,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							Intent intent = new Intent(getActivity(),
									CameraActivity.class);
							intent.putExtra("callingType", CALL_TYPE_REQUEST);
							intent.putExtra("channelName", input.getText()
									.toString());
							startActivity(intent);
						}
					});

			builder.setNegativeButton(android.R.string.cancel,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							dialog.cancel();
						}
					});

			AlertDialog dialog = builder.create();
			dialog.getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
							| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
			dialog.getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

			dialog.show();

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		Intent intent = new Intent(getActivity(), CameraActivity.class);

		String objectId = userChannels.get(position).getObjectId();
		String channelId = userChannels.get(position).getChannelId();
		String channelName = userChannels.get(position).getName();

		intent.putExtra("objectId", objectId);
		intent.putExtra("channelId", channelId);
		intent.putExtra("channelName", channelName);
		intent.putExtra("callingType", CALL_TYPE_RESPONSE);

		startActivity(intent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_ACTIVITY_LOGIN) {
			if (resultCode == RESPONSE_CODE_LOGIN_SUCCESS) {
				getChannelList();
			}
		}
	}

	@Override
	public void onLoggedIn() {
		
	}

	@Override
	public void onLoggedOut() {
		
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.main, menu);
	}
	
}

package com.wj.rocket.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.skt.baas.api.BaasFacebookUtils;
import com.skt.baas.api.BaasInstallation;
import com.skt.baas.api.BaasUser;
import com.skt.baas.callback.BaasLoginCallback;
import com.skt.baas.exception.BaasException;
import com.wj.rocket.GCMIntentService;
import com.wj.rocket.R;
import com.wj.rocket.R.id;
import com.wj.rocket.R.layout;
import com.wj.rocket.base.BaseActivity;
import com.wj.rocket.util.LogUtil;


public class LoginActivity extends BaseActivity {
	
	private Button loginbutton;
	private Button FBSignup;
	private Button createUser;
	
	private EditText password;
	private EditText username;

	String usernametxt;
	String passwordtxt;
	
	private int loginResult;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		loginResult = RESPONSE_CODE_LOGIN_FAIL;
		
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);

		loginbutton = (Button) findViewById(R.id.login);
		FBSignup = (Button) findViewById(R.id.FBSignup);

		loginbutton.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				usernametxt = username.getText().toString();
				passwordtxt = password.getText().toString();
				
				if (usernametxt.equals("")) {
					Toast.makeText(LoginActivity.this, R.string.err_login_empty_id, Toast.LENGTH_LONG).show();
					return;
				} else if (passwordtxt.equals("")) {
					Toast.makeText(LoginActivity.this, R.string.err_login_empty_passwd, Toast.LENGTH_LONG).show();
					return;
				}

				lockUI();
				BaasUser.loginInBackground(usernametxt, passwordtxt, new BaasLoginCallback() {

					@Override
					public void onSuccess(BaasUser user, BaasException e) {
						unLockUI();
						if (e==null) {
							if (user != null) {
								Toast.makeText(LoginActivity.this, R.string.success_login, Toast.LENGTH_LONG).show();
								loginResult = RESPONSE_CODE_LOGIN_SUCCESS;
								finish();
							} else {
								Toast.makeText(getApplicationContext(), R.string.err_login_unknown_user,
										Toast.LENGTH_LONG).show();
							}
						} else {
							Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		});
		
		FBSignup.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				lockUI();
				BaasFacebookUtils.logIn(LoginActivity.this, new BaasLoginCallback() {

					@Override
					public void onSuccess(BaasUser user, BaasException e) {
						unLockUI();
						
						if (e != null) {
							// 실패
							LogUtil.e("FacebookLogin", e.getMessage());
						} else {
							// 성공
							
							if (user == null) {
								Toast.makeText(LoginActivity.this, R.string.error_signup, Toast.LENGTH_LONG).show();
							} else if (user != null) {
								Toast.makeText(LoginActivity.this, R.string.success_login_facebook, Toast.LENGTH_LONG).show();
								try {
									loginResult = RESPONSE_CODE_LOGIN_SUCCESS;
									finish();
								} catch(Exception e1) {
									loginResult = RESPONSE_CODE_LOGIN_FAIL;
									e1.printStackTrace();
								}
							}
						}
					}
				});
			}
		});
		
		createUser = (Button) findViewById(R.id.createUser);
		createUser.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		BaasFacebookUtils.finishAuthentication(requestCode, resultCode, data);
	}

	@Override
	public void finish() {
		setResult(loginResult);
		super.finish();
	}
	
}
package com.wj.rocket.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.ImageView;

import com.wj.rocket.R;
import com.wj.rocket.base.BaseActivity;

public class AboutActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		ImageView ivJwkim = (ImageView) findViewById(R.id.ivJwkim);
		RoundedAvatarDrawable jwKim = new RoundedAvatarDrawable(this, getResources().getDrawable(R.drawable.jwkim));
		ivJwkim.setBackground(jwKim);
		
		ImageView ivMjHan = (ImageView) findViewById(R.id.ivMjhan);
		RoundedAvatarDrawable mjHan = new RoundedAvatarDrawable(this, getResources().getDrawable(R.drawable.mjhan));
		ivMjHan.setBackground(mjHan);
		
	}

}

class RoundedAvatarDrawable extends Drawable {
	
	  private Bitmap mBitmap;
	  private Paint mPaint;
	  private RectF mRectF;
	  private int mBitmapWidth;
	  private int mBitmapHeight;
	 
	  public RoundedAvatarDrawable(Context context, Bitmap bitmap) {
		  init(context, bitmap);
	  }
	  
	  public RoundedAvatarDrawable(Context context, Drawable drawable) {
		  init(context, ((BitmapDrawable) drawable).getBitmap());
	  }
	  
	  private void init(Context context, Bitmap bitmap) {
		  	Resources r = context.getResources();
		    float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, r.getDisplayMetrics());
		    
		  	int viewHeight = (int) (px + 0.5f);
		  	float width = bitmap.getWidth();
		  	float height = bitmap.getHeight();
		  	
		  	if (height > viewHeight) {
		  		float percent = (float) (height / 100);
		  		float scale = (float) (viewHeight / percent);
		  		width *= (scale / 100);
		  		height *= (scale / 100);
		  	}
		  	
		  	mBitmap = Bitmap.createScaledBitmap(bitmap, (int) width, (int) height, true);
		    mRectF = new RectF();
		    mPaint = new Paint();
		    mPaint.setAntiAlias(true);
		    mPaint.setDither(true);
		    final BitmapShader shader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
		    mPaint.setShader(shader);
		 
		    mBitmapWidth = (int) width;
		    mBitmapHeight = (int) height;
	  }
	 
	  @Override
	  public void draw(Canvas canvas) {
	    canvas.drawOval(mRectF, mPaint);
	  }
	 
	  @Override
	  protected void onBoundsChange(Rect bounds) {
	    super.onBoundsChange(bounds);
	 
	    mRectF.set(bounds);
	  }
	 
	  @Override
	  public void setAlpha(int alpha) {
	    if (mPaint.getAlpha() != alpha) {
	      mPaint.setAlpha(alpha);
	      invalidateSelf();
	    }
	  }
	 
	  @Override
	  public void setColorFilter(ColorFilter cf) {
	    mPaint.setColorFilter(cf);
	  }
	 
	  @Override
	  public int getOpacity() {
	    return PixelFormat.TRANSLUCENT;
	  }
	 
	  @Override
	  public int getIntrinsicWidth() {
	    return mBitmapWidth;
	  }
	 
	  @Override
	  public int getIntrinsicHeight() {
	    return mBitmapHeight;
	  }
	 
	  public void setAntiAlias(boolean aa) {
	    mPaint.setAntiAlias(aa);
	    invalidateSelf();
	  }
	 
	  @Override
	  public void setFilterBitmap(boolean filter) {
	    mPaint.setFilterBitmap(filter);
	    invalidateSelf();
	  }
	 
	  @Override
	  public void setDither(boolean dither) {
	    mPaint.setDither(dither);
	    invalidateSelf();
	  }
	 
	  public Bitmap getBitmap() {
	    return mBitmap;
	  }
	 
	}


package com.wj.rocket.activity;

import java.util.regex.Pattern;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.skt.baas.api.BaasUser;
import com.skt.baas.exception.BaasException;
import com.wj.rocket.R;
import com.wj.rocket.base.BaseActivity;


public class SignupActivity extends BaseActivity {

	private Button createUserCancel;
	private Button createUser;
	private EditText password;
	private EditText username;
	private EditText email;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);

		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		email = (EditText) findViewById(R.id.email);
		final EditText repeatPassword = (EditText) findViewById(R.id.etRepeatPassword);
		
		username.setFilters(new InputFilter[] { inputEngNum });
		
		createUser = (Button) findViewById(R.id.createUserBtn);
		createUserCancel = (Button) findViewById(R.id.createUserCancel);

		createUser.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				String userId = username.getText().toString();
				String passwd = password.getText().toString();
				String userEmail = email.getText().toString();
				
				if (userId.isEmpty())
					Toast.makeText(SignupActivity.this, R.string.err_login_empty_id, Toast.LENGTH_LONG).show();
				else if (passwd.isEmpty())
					Toast.makeText(SignupActivity.this, R.string.err_login_empty_passwd, Toast.LENGTH_LONG).show();
				else if (userEmail.isEmpty())
					Toast.makeText(SignupActivity.this, R.string.err_signup_empty_email, Toast.LENGTH_LONG).show();
				else if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())
					Toast.makeText(SignupActivity.this, R.string.err_signup_wrong_email, Toast.LENGTH_LONG).show();
				else {
					
					String repeatPasswd = repeatPassword.getText().toString();
					
					if (repeatPasswd.equals(passwd)) {
						BaasUser newUser = new BaasUser();
						newUser.setUserName(userId);
						newUser.setPassword(passwd);
						newUser.setUserEmail(userEmail);
						
						try {
							newUser.serverSave();
							Toast.makeText(SignupActivity.this, R.string.success_signup, Toast.LENGTH_LONG).show();
							BaasUser.logOut();
							finish();
						} catch (BaasException e) {
							Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
						}
					} else {
						showToast(R.string.err_signup_wrong_repeat_passwd, Toast.LENGTH_LONG, true);
					}
				}
			}
		});

		createUserCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	private InputFilter inputEngNum = new InputFilter() {

		@Override
		public CharSequence filter(CharSequence source, int start, int end,
				Spanned dest, int dstart, int dend) {
			
			  Pattern ps = Pattern.compile("^[a-zA-Z0-9]+$");
              if (!ps.matcher(source).matches()) {
                      return "";
              } 
			
			return null;
		}
		
	};
	
}
package com.wj.rocket.activity;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.FaceDetector;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.skt.baas.api.BaasInstallation;
import com.skt.baas.api.BaasObject;
import com.skt.baas.api.BaasUser;
import com.skt.baas.callback.BaasDeleteCallback;
import com.skt.baas.callback.BaasSaveCallback;
import com.skt.baas.exception.BaasException;
import com.sktelecom.playrtc.exception.RequiredConfigMissingException;
import com.sktelecom.playrtc.exception.RequiredParameterMissingException;
import com.sktelecom.playrtc.exception.UnsupportedPlatformVersionException;
import com.wj.rocket.R;
import com.wj.rocket.base.BaseActivity;
import com.wj.rocket.blackbox.GyroSensorMotion;
import com.wj.rocket.blackbox.OnGyroCallback;
import com.wj.rocket.model.Channel;
import com.wj.rocket.network.BaasServiceHelper;
import com.wj.rocket.playrtc.PlayRTCHandler;
import com.wj.rocket.playrtc.PlayRTCObserverImpl;
import com.wj.rocket.playrtc.VideoViewHandler;
import com.wj.rocket.sound.MicrophoneInput;
import com.wj.rocket.sound.MicrophoneInputListener;
import com.wj.rocket.sound.OnSpeechCallback;
import com.wj.rocket.sound.SoundAnalyzer;
import com.wj.rocket.sound.SoundLevelMeter;
import com.wj.rocket.sound.SoundLevelMeter.SoundLevelMeterListener;
import com.wj.rocket.util.AppUtil;
import com.wj.rocket.util.CloseOnBackPressed;
import com.wj.rocket.util.Constants;
import com.wj.rocket.util.LogUtil;

public class CameraActivity extends BaseActivity implements MicrophoneInputListener, Constants {

	public static final String LOG_TAG = CameraActivity.class.getSimpleName();
	
	private Timer screenshotTimer;
	private TimerTask screenshotTimerTask;
	private Timer pushableTimer;
	private TimerTask pushableTimerTask;

	private PlayRTCHandler playRTCHandler = null;
	private VideoViewHandler viewHandler = null;
	private PlayRTCObserverImpl playRTCPbserver = null;

	private Channel channelData = null;

	// 동작 상태 정의, 연결 종료 및 PlayRTC 객체 해지, Activity 종료 등의 동작을 처리하기 위해 구분
	private int playrtcStat = PLAYRTC_STAT_NONE;

	// 서비스 호출 유형, 초청하는 경우와 요청받은 경우 구분
	public int callingType = CALL_TYPE_REQUEST;

	// 채널생성 중복 요청 방지
	private boolean channelLock = false;

	private boolean dontKillProcess = false;
	private int closeMode = PLAYRTC_CLOSE_NORMAL;

	private CloseOnBackPressed onBackPressedHandler;
	
//	private SoundLevelMeter soundMeter;
	private GyroSensorMotion gyroMotion;
	private SoundAnalyzer soundAnalyzer;
	
	MicrophoneInput micInput;  // The micInput object provides real time audio.
	double mOffsetdB = 10; // Offset for bar, i.e. 0 lit LEDs at 10 dB.
	// The Google ASR input requirements state that audio input sensitivity
	// should be set such that 90 dB SPL at 1000 Hz yields RMS of 2500 for
	// 16-bit samples, i.e. 20 * log_10(2500 / mGain) = 90.
	double mGain = 2500.0 / Math.pow(10.0, 90.0 / 20.0);
	// For displaying error in calibration.
	double mDifferenceFromNominal = 0.0;
	double mRmsSmoothed; // Temporally filtered version of RMS.
	double mAlpha = 0.9; // Coefficient of IIR smoothing filter for RMS.
	private int mSampleRate; // The audio sampling rate to use.
	private int mAudioSource; // The audio source to use.
	  
	
//	private Bitmap capturedImage;
	
	private List<Integer> detectFacesCount;
	
	private boolean pushableStatus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 환경설정
		setConfigurations();

		// 화면 UI 처리
		setCommunicationContentView();

		// AudioManager 설정
		setAudioManager();

		// PlayRTC 생성 config 설정
		initPlayRTC();

		setRegistStatus(PLAYRTC_STAT_NONE);

		if (callingType == CALL_TYPE_REQUEST) {
			createChannel(BaasInstallation.getCurrentInstallation().getInstallationId());
			
			// 얼굴인식 기능 활성화 체크
			if (sharedPreferences.getBoolean(SETTING_ENABLE_DETECT_FACE, false)) {
				startRefreshPushableStatus();
				setTTS();
				startRepeatCapture();
				
			}
			
			// 음성인식 기능 활성화 체크 
			if (sharedPreferences.getBoolean(SETTING_ENABLE_VOICE_RECOGNITION, false)) {
				startRefreshPushableStatus();
//				setSoundMeter();
				
				micInput = new MicrophoneInput(this);
				readPreferences();
				micInput.setSampleRate(mSampleRate);
				micInput.setAudioSource(mAudioSource);
				micInput.start();
		          
			}
			
			// 블랙박스 기능 활성화 체크
			if (sharedPreferences.getBoolean(SETTING_ENABLE_BLACK_BOX, false)) {
				startRefreshPushableStatus();
				setGyroSensorMotion();
			}
			
			//TEST
//			TimerTask testTimerTask = new TimerTask() {
//
//				@Override
//				public void run() {
//					Gson gson = new Gson();
//					String deserializedChannelData = gson.toJson(channelData, Channel.class);
//					
//					BaasServiceHelper.newInstance().sendPushMessage(BaasInstallation.getCurrentInstallation().getObjectId(),
//							TYPE_PUSH_DETECT_INVASION,
//							deserializedChannelData, 
//							new Callback<JSONObject>() {
//
//								@Override
//								public void failure(RetrofitError e) {
//									onError(e);
//								}
//
//								@Override
//								public void success(JSONObject jsonObject, Response response) {
//									LogUtil.d("pushResult", jsonObject.toString());
//								}
//					});
//				}
//				
//			};
//			
//			Timer testTimer = new Timer();
//			testTimer.schedule(testTimerTask, 10000);
			
			
		} else if (callingType == CALL_TYPE_RESPONSE) {
			connectChannel(channelData.getChannelId(), BaasInstallation.getCurrentInstallation().getInstallationId());
		}
	}
	
	/**
	   * Method to read the sample rate and audio source preferences.
	   */
	  private void readPreferences() {
	    SharedPreferences preferences = getSharedPreferences("LevelMeter",
	        MODE_PRIVATE);
	    mSampleRate = preferences.getInt("SampleRate", 8000);
	    mAudioSource = preferences.getInt("AudioSource", 
	        MediaRecorder.AudioSource.VOICE_RECOGNITION);
	  }


	private void setTTS() {
		soundAnalyzer = new SoundAnalyzer(this);
		
	}
	

	private void setGyroSensorMotion() {
		GyroSensorMotion gyro = new GyroSensorMotion(getApplicationContext());
		gyro.setCallback(new OnGyroCallback() {
			
			@Override
			public void OnReceivedCrashed() {
				sendPushDetectCrash();
			}
		});
		gyro.startGyroMotion();
	}

//	private void setSoundMeter() {
//		soundMeter = new SoundLevelMeter();
//		soundMeter.setListener(new SoundLevelMeterListener() {
//
//			@Override
//			public void onMeasureDB(double db) {
//				double convertedDb = db * 10;
//				
//				if (convertedDb >= 600) {
//					LogUtil.d("SoundLevel", String.valueOf(convertedDb) + "db");
//					
//				}
//				
////				if (db >= MAX_DB) {
////					sendPushDetectInvasion();
////				}
//			}
//		});
//		soundMeter.startCheck();
//	}

	private void setConfigurations() {
		channelData = new Channel();
		channelData.setObjectId(getIntent().getStringExtra("objectId"));
		channelData.setUserId(BaasUser.getCurrentUser().getObjectId());
		channelData.setName(getIntent().getStringExtra("channelName"));

		callingType = getIntent().getIntExtra("callingType", CALL_TYPE_REQUEST);
		if (callingType == CALL_TYPE_RESPONSE) {
			channelData.setChannelId(getIntent().getStringExtra("channelId"));
		}
		
	}
	
	private void startRefreshPushableStatus() {
		pushableTimerTask = new TimerTask() {

			@Override
			public void run() {
				if (!pushableStatus)
					pushableStatus = true;
			}
			
		};
		
		pushableTimer = new Timer();
		pushableTimer.schedule(pushableTimerTask, 5000, 600000);
	}
	
	private void startRepeatCapture() {
        screenshotTimerTask = new TimerTask() {
        	
        	@Override
        	public void run() {
        		if (detectFacesCount == null)
        			detectFacesCount = new ArrayList<Integer>();
        		
        		final String imagePath = captureSrcreen();
//        		if (detectFaces(imagePath) >= 1) {
//        			Gson gson = new Gson();
//        			String deserializedChannelData = gson.toJson(channelData, Channel.class);
//        			
//        			BaasServiceHelper.newInstance().sendPushMessage(BaasInstallation.getCurrentInstallation().getObjectId(),
//							TYPE_PUSH_DETECT_MAX_FACES,
//							deserializedChannelData, 
//							new Callback<JSONObject>() {
//
//								@Override
//								public void failure(RetrofitError e) {
//									onError(e);
//								}
//
//								@Override
//								public void success(JSONObject jsonObject, Response response) {
//									LogUtil.d("pushResult", jsonObject.toString());
//								}
//					});
//        			
//        		}
        			
        		detectFacesCount.add(detectFaces(imagePath));
        		final int isInvasion = detectInvasion();
        		
        		runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						switch (isInvasion) {
						case DETECTED_INVASION:
							// 침입이 탐지된 경우
							
//							if (soundMeter != null) {
//								soundMeter.pause();	
//							}
							
							if (micInput != null) {
								micInput.stop();
							}
							
							soundAnalyzer.setSpeechCallback(new OnSpeechCallback() {
									
									@Override
									public void onReceivedResponse() {
										sendPushDetectInvasion();
									}
							});
							soundAnalyzer.startTTS();
							
						case NOT_DETECTED_INVASION:
							// 침입이 탐지되지 않은 경우 
						case PERFORMED_DETECT_INVASION:
							// 침입탐지 기능이 수행되었으나 아직 결과를 내놓지 못한 경우
						}
					}
				});
        	}
        };
        
        screenshotTimer = new Timer();
        screenshotTimer.schedule(screenshotTimerTask, 5000, 1000);
        
	}
	
	private void sendPushDetectInvasion() {
		if (pushableStatus) {
			pushableStatus = false;
			
			Gson gson = new Gson();
			String deserializedChannelData = gson.toJson(channelData, Channel.class);
			
			BaasServiceHelper.newInstance().sendPushMessage(BaasInstallation.getCurrentInstallation().getObjectId(),
					TYPE_PUSH_DETECT_INVASION,
					deserializedChannelData, 
					new Callback<JSONObject>() {

						@Override
						public void failure(RetrofitError e) {
							onError(e);
						}

						@Override
						public void success(JSONObject jsonObject, Response response) {
							LogUtil.d("pushResult", jsonObject.toString());
						}
			});
		}
	}
	
	private void sendPushDetectCrash() {
		if (pushableStatus) {
			pushableStatus = false;
			
			Gson gson = new Gson();
			String deserializedChannelData = gson.toJson(channelData, Channel.class);
			
			BaasServiceHelper.newInstance().sendPushMessage(BaasInstallation.getCurrentInstallation().getObjectId(),
					TYPE_PUSH_DETECT_CRASH,
					deserializedChannelData, 
					new Callback<JSONObject>() {

						@Override
						public void failure(RetrofitError e) {
							onError(e);
						}

						@Override
						public void success(JSONObject jsonObject, Response response) {
							LogUtil.d("pushResult", jsonObject.toString());
						}
			});
		}
		
	}
	
	private String captureSrcreen() {
		StringBuilder sb = new StringBuilder();
		sb.append(getFilesDir());
		sb.append("/");
		sb.append(Math.abs(UUID.randomUUID().hashCode()));
		sb.append(".png");
		String filePath = sb.toString();
		
		DataOutputStream os = null;
		
		try {
			Process sh = Runtime.getRuntime().exec("su", null, null);
			os = new DataOutputStream(sh.getOutputStream());
			
			os.writeBytes("/system/bin/screencap -p " + filePath + "\n");
			os.writeBytes("/system/bin/chmod 644 " + filePath + "\n");
//			LogUtil.d("ProcessId", String.valueOf(android.os.Process.));
//			os.write(("/system/bin/chown " + String.valueOf(android.os.Process.myUid()) + filePath).getBytes("ASCII"));
			os.writeBytes("exit\n");
			os.flush();
			os.close();
			sh.waitFor();
			
		} catch (Exception e) {
			if (DEBUG)
				e.printStackTrace();
		} finally {
			if (os != null)
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
		return filePath;
		
	}
	
	private int detectFaces(String imageFile) {
		Bitmap screen = BitmapFactory.decodeFile(imageFile);
		
		FaceDetector fd = null;
		FaceDetector.Face[] faces = new FaceDetector.Face[MAX_FACES];
		int count = 0;

		try {
			fd = new FaceDetector(screen.getWidth(), screen.getHeight(),
					MAX_FACES);
			count = fd.findFaces(screen, faces);
		} catch (Exception e) {
			if (DEBUG)
				e.printStackTrace();
		}
		
		LogUtil.d("ScreenShot", "detected face: " + Integer.toString(count));
		(new File(imageFile)).delete();
		
		return count;
		
	}
	
	private int detectInvasion() {
		if (detectFacesCount.size() >= MAX_FACE_DETECT_COUNT) {
			int average = 0;
			int total = 0;
			
			for (int i = 0; i < detectFacesCount.size(); i++) {
				total += detectFacesCount.get(i);
			}
			
			if (total != 0) 
				average = total / detectFacesCount.size();
			
			detectFacesCount.clear();
			
			if (average == MAX_FACES)
				return DETECTED_INVASION;
			else
				return NOT_DETECTED_INVASION;
		}
		return PERFORMED_DETECT_INVASION;
	}
	
	@Override
	public void onDestroy() {
		if (screenshotTimer != null)
			screenshotTimer.cancel();
		
//		if (soundMeter != null)
//			soundMeter.stop();
		
		if (micInput != null) {
			micInput.stop();
		}

		if (gyroMotion != null)
			gyroMotion.stoptGyroMotion();

		if (soundAnalyzer != null)
			soundAnalyzer.stopTTS();
		
		super.onDestroy();
	}

	// Activty의 포커스 여부를 확인
	// 영상 스트림 출력을 위한 PlayRTCVideoView(GLSurfaceView를 상속) 동적 코드 생성
	// 생성 시 스크린 사이즈를 생성자에 넘김
	// hasFocus = true , 화면보여짐 , onCreate | onResume
	// hasFocus = false , 화면안보임 , onPause | onDestory
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		LogUtil.d(LOG_TAG, "Activity =>> onWindowFocusChanged hasFocus[%s]", "" + hasFocus);
		if (hasFocus && viewHandler.hasStreamView() == false) {

			createVideoStreamView();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		LogUtil.i(LOG_TAG, "Activity =>> onPause");
		// PlayRTC 객체에 onPause 이벤트 전달
		if (playRTCHandler != null)
			playRTCHandler.pause();
	}

	@Override
	public void onResume() {
		super.onResume();
		LogUtil.i(LOG_TAG, "Activity =>> onResume");
		// PlayRTC 객체에 onResume 이벤트 전달
		if (playRTCHandler != null)
			playRTCHandler.resume();
	}

	@Override
	public void onBackPressed() {
		boolean isCloesAction = checkRegistStatus(PLAYRTC_STAT_DISCONNECTING);
		LogUtil.i(LOG_TAG, "Activity =>> onBackPressed isCloesAction[" + isCloesAction + "]");

		if (onBackPressedHandler.onBackPressed()) {
			finish();
		}
	}

	@Override
	public void finish() {
		if (checkRegistStatus(PLAYRTC_STAT_CONNECTED)) {
			setRegistStatus(PLAYRTC_STAT_DISCONNECTING);
			if (callingType == CALL_TYPE_REQUEST) {
				deleteChannel(new BaasDeleteCallback() {

					@Override
					public void onSuccess(BaasException e) {
						BaasServiceHelper.newInstance().sendPushMessage(BaasInstallation.getCurrentInstallation().getObjectId(),
								TYPE_PUSH_REFRESH_CHANNELS,
								"",
								new Callback<JSONObject>() {

									@Override
									public void failure(RetrofitError e) {
										onError(e);
									}

									@Override
									public void success(JSONObject jsonObject, Response response) {
										LogUtil.d("pushResult", jsonObject.toString());
										CameraActivity.super.finish();
									}
						});
					}
				});
			} else {
				disconnectChannel();
				super.finish();
			}
		} else {
			super.finish();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			incrementMasterVolume(true);
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
			incrementMasterVolume(false);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void saveChannelId(final String channelId) {
		channelData.setChannelId(channelId);

		final BaasObject channel = new BaasObject("Channel");
		channel.set("name", channelData.getName());
		channel.set("channelId", channelId);
		channel.set("userId", BaasUser.getCurrentUser().getObjectId());
		channel.serverSaveInBackground(new BaasSaveCallback() {
				@Override
				public void onSuccess(BaasException e) {
					if (e != null) {
						// 에러 처리
					}
					channelData.setObjectId(channel.getObjectId());
					
					Gson gson = new Gson();
        			String deserializedChannelData = gson.toJson(channelData, Channel.class);
        			
					BaasServiceHelper.newInstance().sendPushMessage(BaasInstallation.getCurrentInstallation().getObjectId(),
							TYPE_PUSH_REFRESH_CHANNELS,
							deserializedChannelData,
							new Callback<JSONObject>() {

								@Override
								public void failure(RetrofitError e) {
									onError(e);
								}

								@Override
								public void success(JSONObject jsonObject, Response response) {
									LogUtil.d("pushResult", jsonObject.toString());
								}
					});
				channelData.setObjectId(channel.getObjectId());
			}
		});

		// // 사용자 유형이 사용자(도움요청자)이면 음성안내
		// if (isHelperUser() == false) {
		// app.textToSpeech(getString(R.string.sound_send_invite), null);
		// }
		// final String myId = userInfo.getUserId();
		// // Push 메세지
		// String inviteMsg = String
		// .format("{\\\"type\\\":\\\"calling\\\",\\\"caller\\\":\\\"%s\\\",\\\"callee\\\":\\\"%s\\\",\\\"channelId\\\":\\\"%s\\\",\\\"sendtime\\\":%s}",
		// myId, calleeId, channelId,
		// "" + System.currentTimeMillis());
		// final BaasServiceHelper apiHelper = BaasServiceHelper.getInstance();
		//
		// apiHelper.sendPushMessage(userInfo.getFriendInstallationId(),
		// inviteMsg, new PushListener() {
		//
		// // 초청 메세지 전송 응답 수신 처리
		// @Override
		// public void onPushResult(boolean success) {
		// if (success == false) {
		// // 초청 메세지 전송 성공하면 PlayRTC Service Wrapper 서비스에서 서비스
		// // 설정을 조회한다.
		// // 사용자 유형이 사용자(도움요청자)이면 재시도 음성안내 출력
		// if (isHelperUser() == false) {
		// app.textToSpeech(
		// getString(R.string.sound_invite_fail_retry),
		// null);
		// } else {
		//
		// // 사용자 유형이 도우미이면 재시도 안내 AlertDialog 출력
		// AlertDialog.Builder alert = new AlertDialog.Builder(
		// CommunicationActivity.this);
		// alert.setTitle(getString(R.string.app_name));
		// alert.setMessage(getString(R.string.dlg_msg_invite_fail_retry));
		// alert.setPositiveButton(
		// getString(R.string.dlg_btn_ok),
		// new DialogInterface.OnClickListener() {
		// public void onClick(
		// DialogInterface dialog,
		// int which) {
		// dialog.dismiss();
		// sendInviteFriendPushMessage(channelId);
		// }
		// });
		// alert.setNegativeButton(
		// getString(R.string.dlg_btn_cancel),
		// new DialogInterface.OnClickListener() {
		// public void onClick(
		// DialogInterface dialog,
		// int whichButton) {
		// dialog.dismiss();
		// titleBar.showSettingButton(true);
		// }
		// });
		// alert.show();
		// }
		// }
		//
		// }
		//
		// // 초청 메세지 전송 오류 처리
		// @Override
		// public void onPushFail(String code, String desc) {
		//
		// // 사용자 유형이 사용자(도움요청자)이면 실패/종료 음성안내 출력
		// if (callUserType == CommunicationActivity.USER_TYPE_USER) {
		// app.textToSpeech(
		// getString(R.string.sound_invite_fail_close),
		// new TTSCompletListener() {
		//
		// @Override
		// public void onCompletion() {
		// setRegistStatus(PLAYRTC_STAT_DISCONNECTING);
		// deleteChannel();
		// }
		//
		// });
		//
		// } else {
		// // 사용자 유형이 도우미이면 실패/종료 안내 AlertDialog 출력
		// String msg = getString(R.string.dlg_msg_invite_fail_close);
		// String btn = getString(R.string.dlg_btn_ok);
		// showAlertDialog(msg, btn, false, true, true);
		//
		// }
		// }
		//
		// });
	}

	public void restoreMasterVolume() {
		int volume = sharedPreferences.getInt("old_volume", 1);
		AppUtil.setMasterVolume(this, volume);

	}

	private void createVideoStreamView() {
		viewHandler.createVideoStreamView();
	}

	private void setCommunicationContentView() {
		onBackPressedHandler = new CloseOnBackPressed(this);

		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_camera);

		viewHandler = new VideoViewHandler(this);
	}

	public void setAudioManager() {
		/* Audio 설정 */
		// WiredHeadset/블루투스를 사용하는지 에 따라 적절한 설정을 해주세요
		AudioManager audioManager = ((AudioManager) getSystemService(AUDIO_SERVICE));
		// suppression.
		@SuppressWarnings("deprecation")
		boolean isWiredHeadsetOn = audioManager.isWiredHeadsetOn();
		boolean isBluetoothOn = audioManager.isBluetoothScoOn();
		int audioMode = isWiredHeadsetOn ? AudioManager.MODE_IN_CALL : // AudioManager
																		// 를
																		// 전화모드로
				AudioManager.MODE_IN_COMMUNICATION;
		audioManager.setMode(audioMode);
		audioManager.setSpeakerphoneOn(!(isWiredHeadsetOn || isBluetoothOn));

		Editor editor = sharedPreferences.edit();
		editor.putInt("old_volume", audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
		editor.commit();
				
	}

	public void createChannel(String uid) {
		if (channelLock == true) {
			return;
		}
		LogUtil.i(LOG_TAG, "createChannel uid[%s]", uid);
		channelLock = true;

		setRegistStatus(PLAYRTC_STAT_CONNECTED);
		try {
			playRTCHandler.createCommunicationChannel(uid);
		} catch (RequiredConfigMissingException e) {
			e.printStackTrace();
		}

	}

	private void connectChannel(String channelId, String uid) {
		if (channelLock == true) {
			return;
		}
		LogUtil.i(LOG_TAG, "connectChannel channelId[%s] uid[%s]", channelId, uid);
		channelLock = true;
		// 연결 요청 메세지 전송 재시도
		setRegistStatus(PLAYRTC_STAT_CONNECTED);
		try {
			playRTCHandler.connectCommunicationChannel(channelId, uid);
		} catch (RequiredConfigMissingException e) {
			e.printStackTrace();
		}

	}

	private void disconnectChannel() {
		if (playRTCHandler != null) {
			playRTCHandler.disconnectCommunicationChannel();
		}
	}

	private void deleteChannel(BaasDeleteCallback callBack) {
		if (playRTCHandler != null) {
			playRTCHandler.deleteCommunicationChannel();
		}
		
		BaasObject baasObject = new BaasObject("Channel");
		baasObject.setObjectId(channelData.getObjectId());
		baasObject.serverDeleteInBackground(callBack);
	}

	public void resetPlayRTCAndReady() {
		LogUtil.d("PLAYRTC", "clear and reset PlayRTC....");
		deleteChannel(new BaasDeleteCallback() {

			@Override
			public void onSuccess(BaasException arg0) {
				playRTCHandler = null;
				initPlayRTC();
				channelLock = false;
				createChannel(BaasInstallation.getCurrentInstallation().getInstallationId());
			}
		});
	}

	/*
	 * PlayRTC 객체 생성 후 서비스 설정을 한다. 1. PlayRTC 서비스 설정 객체 생성 2. PlayRTC 생성, 생성자에
	 * 서비스 설정 객체 전달 3. PlayRTC의 join 메소드를 호출하여 PlayRTC 채널 서버에 접속.
	 */
	private void initPlayRTC() {
		LogUtil.d("PLAYRTC", "initPlayRTC...");

		// 영상 스트림 수신 시 영상 출력을 하기 위해 영상 렌더러 구현 객체를 전달
		playRTCPbserver = new PlayRTCObserverImpl(this, viewHandler);

		// PlayRTC Handler 인스턴스 생성
		try {
			// playRTCHandler = new PlayRTCHandler((Activity) this,
			// URL_PLAY_RTC_SERVICE_SERVER, playRTCPbserver);
			playRTCHandler = new PlayRTCHandler((Activity) this, playRTCPbserver);
		} catch (UnsupportedPlatformVersionException e) {
			if (DEBUG)
				e.printStackTrace();
		} catch (RequiredParameterMissingException e) {
			if (DEBUG)
				e.printStackTrace();
		}

		if (callingType == CALL_TYPE_REQUEST) {
			playRTCHandler.setConfiguration("back", true);
		} else {
			playRTCHandler.setConfiguration("back", false);
		}

	}

	// Master Volume 조정
	private void incrementMasterVolume(boolean isUp) {
		int playVol = AppUtil.getMasterVolume(CameraActivity.this);
		if (isUp)
			playVol += 1;
		else
			playVol -= 1;
		AppUtil.setMasterVolume(CameraActivity.this, playVol);
	}

	public void setRegistStatus(int status) {
		playrtcStat |= status;
	}

	public void removeRegistStatus(int status) {
		playrtcStat &= ~status;
	}

	public boolean checkRegistStatus(int status) {
		int val = (playrtcStat & status);
		return val == status;
	}

	public boolean isClosePause() {
		return closeMode == PLAYRTC_CLOSE_PAUSE;
	}

	public boolean isCloseForcing() {
		return closeMode == PLAYRTC_CLOSE_FORCING;
	}

	public boolean hasChannelId() {
		return TextUtils.isEmpty(channelData.getChannelId()) == false;
	}

	@Override
	protected void onUserLeaveHint() {
		LogUtil.i(LOG_TAG, "Activity =>> onUserLeaveHint");

		if (isClosePause() == false) {

			LogUtil.d(LOG_TAG, "Activity =>> onUserLeaveHint dontKillProcess=" + dontKillProcess);
			closeMode = PLAYRTC_CLOSE_FORCING;
			super.onBackPressed();
		}
	}

	@Override
	public void processAudioFrame(short[] audioFrame) {
		// Compute the RMS value. (Note that this does not remove DC).
	      double rms = 0;
	      for (int i = 0; i < audioFrame.length; i++) {
	        rms += audioFrame[i]*audioFrame[i];
	      }
	      rms = Math.sqrt(rms/audioFrame.length);

	      // Compute a smoothed version for less flickering of the display.
	      mRmsSmoothed = mRmsSmoothed * mAlpha + (1 - mAlpha) * rms;
	      final double rmsdB = 20.0 * Math.log10(mGain * mRmsSmoothed);
	      
	      DecimalFormat df = new DecimalFormat("##");
	      df.format(20 + rmsdB);
	      
	      if (DEBUG)
	    	  	Log.d("decibel", df.format(20 + rmsdB));
	      
	      if (Integer.valueOf(df.format(20 + rmsdB)) > 75) {
	    	  		sendPushDetectInvasion();
	      }
	}

}

package com.wj.rocket.network;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

import com.skt.baas.api.BaasInstallation;
import com.skt.baas.api.BaasUser;
import com.wj.rocket.util.Constants;

public class BaasServiceHelper implements Constants {
	private static final String LOG_TAG = BaasServiceHelper.class.getSimpleName();
	
	private static BaasServiceHelper instance = null;
	
	// 데이터 전문 처리 시 오류 코드 및 설명 
	private static final String RES_INVALID_DATA_CODE 		= "10000";
	private static final String RES_INVALID_DATA_DESC		= "수신한 데이터를 처리 과정에서 오류가 발생하였습니다.";
	
	// 서비스 Push GCM-RegistrationID 등록 Request 
	private static final String SERVICE_API_INSTALLATION 	= "installation";
	// 서비스 Push GCM-RegistrationID ObjectID 조회  Request 
	private static final String SERVICE_API_GETINSTALLATION = "getinstallation";
	// 서비스 Push GCM-RegistrationID 수정  Request 
	private static final String SERVICE_API_MODINSTALLATION = "updateinstallation";
	// 서비스 Push GCM-RegistrationID 등록 해제 Request 
	private static final String SERVICE_API_UNINSTALLATION 	= "uninstallation";
	// 서비스 Push 메세지 전송 Request 
	private static final String SERVICE_API_PUSHMSG 		= "pushmsg";
	// T-DEV NooNooN 프로젝트 BAAS REST Key 
	private static final String BAAS_TDCPROJECT_KEY			= API_KEY_BAAS;
	// SKT-DEV NooNooN Bass 서비스 URL 
	private static final String BAAS_HTTPS_DOMAIN 			= "https://apis.sktelecom.com";
	
	public static BaasServiceHelper newInstance() {
		if (instance == null) {
			instance = new BaasServiceHelper();
		}
		return instance;
	}
	
	RequestInterceptor requestInterceptor = new RequestInterceptor() {
		  @Override
		  public void intercept(RequestFacade request) {
		    request.addHeader("TDCProjectKey", BAAS_TDCPROJECT_KEY);
		    request.addHeader("Content-Type", "application/json; charset=utf-8");
		  }
		};

	RestAdapter restAdapter = new RestAdapter.Builder()
	  .setEndpoint(BAAS_HTTPS_DOMAIN)
	  .setRequestInterceptor(requestInterceptor)
//	  .setLogLevel(LogLevel.FULL)
	  .build();
	
	BaasService baasService = restAdapter.create(BaasService.class);

	public interface BaasService {
		
		@POST("/v1/baas/installations")
		void createDeviceInstallation(@Body BaasDeviceInstallation deviceData, Callback<BaasDeviceInstallation> cb);
		
		@GET("/v1/baas/data/installations")
		void getDeviceInstallationID(@Query("where") String where, Callback<BaasDeviceInstallation> cb);
		
		@PUT("/v1/baas/installations/{objectId}")
		void deviceUpdateInstallation(@Path("objectId") String objectId, @Body BaasDeviceInstallation deviceData, Callback<BaasDeviceInstallation> cb);
		
		@POST("/v1/push/pushes")
		void sendPushMessage(@Body BaasPush pushData, Callback<JSONObject> cb);
	}
	
	public void createDeviceInstallation(String gcmRegistId, final String deviceUuid, Callback<BaasDeviceInstallation> cb) {
		BaasDeviceInstallation deviceData = new BaasDeviceInstallation();
		deviceData.setDeviceType("android");
		deviceData.setDeviceToken(gcmRegistId);
		deviceData.setInstallationId(deviceUuid);
		baasService.createDeviceInstallation(deviceData, cb);
	}
	
	public void createDeviceInstallation(String channel, String gcmRegistId, final String deviceUuid, Callback<BaasDeviceInstallation> cb) {
		BaasDeviceInstallation deviceData = new BaasDeviceInstallation();
		deviceData.setDeviceType("android");
		deviceData.setDeviceToken(gcmRegistId);
		deviceData.setInstallationId(deviceUuid);
		deviceData.setChannels(new String[]{ channel });
		baasService.createDeviceInstallation(deviceData, cb);
	}
	
	public void getDeviceInstallationID(String deviceUuid, Callback<BaasDeviceInstallation> cb) {
		String where = String.format("{\"installationId\":{\"$like\":{\"__contains\":\"%s\"}}}", deviceUuid);
		baasService.getDeviceInstallationID(where, cb);
	}
	
	public void deviceUpdateInstallation(String objectId, String gcmRegistId, Callback<BaasDeviceInstallation> cb) {
		BaasDeviceInstallation deviceData = new BaasDeviceInstallation();
		deviceData.setDeviceToken(gcmRegistId);
		baasService.deviceUpdateInstallation(objectId, deviceData, cb);
	}
	
	public void deviceUpdateInstallation(String objectId, String gcmRegistId, String channel, Callback<BaasDeviceInstallation> cb) {
		BaasDeviceInstallation deviceData = new BaasDeviceInstallation();
		deviceData.setDeviceToken(gcmRegistId);
		deviceData.setChannels(new String[]{ channel });
		baasService.deviceUpdateInstallation(objectId, deviceData, cb);
	}
	
	public void sendPushMessage(String installationId, String type, String sendMessage, Callback<JSONObject> cb) {
		BaasPush pushData = new BaasPush();
//		pushData.setWhere("android", installationId);
		pushData.setData(type, sendMessage);
		pushData.setChannels(new String[]{ BaasUser.getCurrentUser().getObjectId() });
		baasService.sendPushMessage(pushData, cb);
	}
//	
//	public void sendPushMessage(String installationId, String sendTitle, String type, String sendMessage, Callback<JSONObject> cb) {
//		BaasPush pushData = new BaasPush();
//		pushData.setWhere("android", installationId);
//		pushData.setData(sendTitle, new PushBody(type, sendMessage));
//		pushData.setChannels(new String[]{ BaasInstallation.getCurrentInstallation().getObjectId() });
//		baasService.sendPushMessage(pushData, cb);
//	}
	
}

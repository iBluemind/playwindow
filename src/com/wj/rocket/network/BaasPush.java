package com.wj.rocket.network;


class BaasPushData {
	
	private String title;
	private String alert;
	
	BaasPushData(String alert) {
		this.alert = alert;
	}
	
	BaasPushData(String title, String alert) {
		this.title = title;
		this.alert = alert;
	}
}

public class BaasPush {
	
	private String[] channels;
	private BaasDeviceInstallation where;
	private BaasPushData data;
	
	public BaasDeviceInstallation getWhere() {
		return where;
	}
	public void setWhere(String deviceType, String objectId) {
		BaasDeviceInstallation deviceData = new BaasDeviceInstallation();
		deviceData.setDeviceType(deviceType);
		deviceData.setObjectId(objectId);
		this.where = deviceData;
	}
	public BaasPushData getData() {
		return data;
	}
	public void setData(String alert) {
		this.data = new BaasPushData(alert);
	}
	public void setData(String title, String alert) {
		this.data = new BaasPushData(title, alert);
	}
	public String[] getChannels() {
		return channels;
	}
	public void setChannels(String[] channels) {
		this.channels = channels;
	}

}

package com.wj.rocket.util;

public interface OnLoginListener {
	
	void onLoggedIn();
	void onLoggedOut();

}

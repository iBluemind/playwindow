package com.wj.rocket.util;

import com.wj.rocket.BuildConfig;

public interface Constants {

	public static boolean DEBUG = false;
	
	public static final String API_KEY_BAAS = "6bfb5afd-d1db-40b1-a64d-85e44803b44e";
	public static final String NUMBER_GCM_PROJECT = "141478823591";
	
	public static final String NAME_PLAYREC_SHARED_PREFERENCE = "playrec_configs";
	
	public static final String URL_PLAY_RTC_SERVICE_SERVER = "http://server.manjong.org:5400";

	// 상대에게 연결 요청을 보내는 경우
	public static final int CALL_TYPE_REQUEST = 1;
	// 상대방의 요청을 받아 연결하는 경우
	public static final int CALL_TYPE_RESPONSE = 2;

	// 응답 대기 시간 정의
	public static final long DISCOUNT_TIME = 60 * 1000; // 60 sec

	// PlayRTC P2P 음성 출력 볼륨
	public static final int PLAYRTC_VOICE_VOLUME = 70;

	public static final int PLAYRTC_STAT_NONE = 0;
	public static final int PLAYRTC_STAT_CONNECTED = (1 << 1);
	public static final int PLAYRTC_STAT_DISCONNECTING = (1 << 3);
	public static final int PLAYRTC_STAT_ERROR = (1 << 2);

	public static final int PLAYRTC_CLOSE_PAUSE = 0;
	public static final int PLAYRTC_CLOSE_NORMAL = 1;
	public static final int PLAYRTC_CLOSE_FORCING = 2;
	
	public static final int REQUEST_CODE_ACTIVITY_LOGIN = 0;
	
	public static final int RESPONSE_CODE_LOGIN_SUCCESS = 200;
	public static final int RESPONSE_CODE_LOGIN_FAIL = 300;
	
	public static final int MAX_FACES = 2;
	public static final int MAX_FACE_DETECT_COUNT = 3;
	public static final double MAX_DB = 67;
	
	public static final int DETECTED_INVASION = 1;		// 침입탐지
	public static final int NOT_DETECTED_INVASION = -1;	// 침입이 탐지되지 않음 
	public static final int PERFORMED_DETECT_INVASION = 0;	// 침입탐지 기능이 수행되기만 함. 아직 결과를 내놓지는 못한 상태.
	
	public static final String TYPE_PUSH_REFRESH_CHANNELS = "refresh_channel_list";
	public static final String TYPE_PUSH_DETECT_INVASION = "detect_invasion";
	public static final String TYPE_PUSH_DETECT_CRASH = "detect_crash";
	
	public static final String INTENT_REFRESH_CHANNEL_LIST = "com.wj.rocket.REFRESH_CHANNEL_LIST";
	
	public static final String SETTING_ENABLE_DETECT_FACE = "detect_face_enabled";
	public static final String SETTING_ENABLE_VOICE_RECOGNITION = "void_recognition_enabled";
	public static final String SETTING_ENABLE_BLACK_BOX = "black_box_enabled";
	
}

package com.wj.rocket.util;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wj.rocket.base.BaseFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
	
	private List<BaseFragment> fragmentList;
	
	public SectionsPagerAdapter(FragmentManager fm, List<BaseFragment> fragmentList) {
		super(fm);
		this.fragmentList = fragmentList;
	}

    public void setFragmentList(List<BaseFragment> fragmentList) {
        this.fragmentList = fragmentList;
    }

    public List<BaseFragment> getFragmentList() {
        return fragmentList;
    }

    @Override
	public android.support.v4.app.Fragment getItem(int position) {
		return fragmentList.get(position);
	}

	@Override
	public int getCount() {
		return fragmentList.size();
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return fragmentList.get(position).getTitle();
	}

}

package com.wj.rocket.util;

import android.util.Log;

public class LogUtil {
	public static final int VERBOSE = Log.VERBOSE;
    public static final int DEBUG = Log.DEBUG;
    public static final int INFO = Log.INFO;
    public static final int WARN = Log.WARN;
    public static final int ERROR = Log.ERROR;
    
    public static int loglevel = Log.DEBUG;
    
    /**
     * 로그 레벨을 지정
     *
     * @author Jin-Soo, Kim
     * @param logLevel int
     * @return
     */
    public static void setLogLevel(int level) {
    	loglevel = level;
		Log.i("LogUtil", "Changing log level to " + loglevel);
    }
    
        
    /**
     * Determine if log level will be logged
     *
     * @author Jin-Soo, Kim
     * @param logLevel int
     * @return
     */
    public static boolean isLoggable(int level) {
    	return loglevel >= level && Constants.DEBUG;
    }
    
    /**
     * Verbose log message.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     */
    public static void v(String tag, String s) {
        if (LogUtil.VERBOSE >= loglevel && Constants.DEBUG) Log.v(tag, s);
    }

    /**
     * Debug log message.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     */
    public static void d(String tag, String s) {
        if (LogUtil.DEBUG >= loglevel && Constants.DEBUG) Log.d(tag, s);
    }

    /**
     * Info log message.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     */
    public static void i(String tag, String s) {
        if (LogUtil.INFO >= loglevel && Constants.DEBUG) Log.i(tag, s);
    }

    /**
     * Warning log message.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     */
    public static void w(String tag, String s) {
        if (LogUtil.WARN >= loglevel && Constants.DEBUG) Log.w(tag, s);
    }

    /**
     * Error log message.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     */
    public static void e(String tag, String s) {
        if (LogUtil.ERROR >= loglevel && Constants.DEBUG) Log.e(tag, s);
    }
    
    
    /**
     * Verbose log message with printf formatting.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     * @param args Object...
     */
    public static void v(String tag, String s, Object... args) {
        if (LogUtil.VERBOSE >= loglevel && Constants.DEBUG) Log.v(tag, String.format(s, args));
    }

    /**
     * Debug log message with printf formatting.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     * @param args Object...
     */
    public static void d(String tag, String s, Object... args) {
        if (LogUtil.DEBUG >= loglevel && Constants.DEBUG) Log.d(tag, String.format(s, args));
    }

    /**
     * Info log message with printf formatting.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     * @param args Object...
     */
    public static void i(String tag, String s, Object... args) {
        if (LogUtil.INFO >= loglevel && Constants.DEBUG) Log.i(tag, String.format(s, args));
    }

    /**
     * Warning log message with printf formatting.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     * @param args Object...
     */
    public static void w(String tag, String s, Object... args) {
        if (LogUtil.WARN >= loglevel && Constants.DEBUG) Log.w(tag, String.format(s, args));
    }

    /**
     * Error log message with printf formatting.
     *
     * @author Jin-Soo, Kim
     * @param tag String
     * @param s String
     * @param args Object...
     */
    public static void e(String tag, String s, Object... args) {
        if (LogUtil.ERROR >= loglevel && Constants.DEBUG) Log.e(tag, String.format(s, args));
    }
}

package com.wj.rocket.util;

import android.widget.Toast;

import com.wj.rocket.R;
import com.wj.rocket.base.BaseActivity;

public class CloseOnBackPressed {

    private long backPressedTime = 0;
    private BaseActivity activity;

    public CloseOnBackPressed(BaseActivity activity) {
    		this.activity = activity;

    }

    public boolean onBackPressed() {

        if (System.currentTimeMillis() <= backPressedTime + 2000) {
            return true;
        }

        backPressedTime = System.currentTimeMillis();
        showGuide();

        return false;

    }

    private void showGuide() {
    		activity.showToast(R.string.close_on_back_pressed, Toast.LENGTH_SHORT, true);

    }

}

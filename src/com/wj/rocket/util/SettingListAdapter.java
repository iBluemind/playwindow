package com.wj.rocket.util;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.wj.rocket.R;
import com.wj.rocket.model.Setting;

public class SettingListAdapter extends BaseAdapter {

    private Context context;
    private List<Setting> settingList;
    private LayoutInflater layoutInflater;

    public SettingListAdapter(Context context, List<Setting> settingList) {
        this.context = context;
        this.settingList = settingList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return settingList.size();
    }

    @Override
    public Object getItem(int i) {
        return settingList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemViewType(int position) {
        return settingList.get(position).getType();
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public boolean isEnabled(int position) {
        Setting setting = settingList.get(position);
        return setting.getType() != Setting.TYPE_SETTING_ENTRY_HEADER;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Setting setting = settingList.get(position);

        if (setting != null) {
            int viewResourceId = R.layout.item_setting_list;
            switch (getItemViewType(position)) {
                case Setting.TYPE_SETTING_ENTRY_TOGGLE:
                    viewResourceId = R.layout.item_setting_toggle_list;
                    break;
                case Setting.TYPE_SETTING_ENTRY_HEADER:
                    viewResourceId = R.layout.item_setting_header_list;
                    break;
                default:

            }
            convertView = layoutInflater.inflate(viewResourceId, null);

            TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            tvTitle.setText(setting.getTitle());

            switch (setting.getType()) {
                case Setting.TYPE_SETTING_ENTRY_COMMON:
                case Setting.TYPE_SETTING_ENTRY_TOGGLE:
                    if (setting.getDesc() != null) {
                        TextView tvDesc = (TextView) convertView.findViewById(R.id.tvDesc);
                        tvDesc.setText(setting.getDesc());

                    }
                    break;
            }

            if (setting.getType() == Setting.TYPE_SETTING_ENTRY_TOGGLE) {
                CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
                checkBox.setFocusable(false);
                checkBox.setClickable(false);
                checkBox.setChecked(Boolean.parseBoolean(setting.getValue()));
            }

        }

        return convertView;
    }
}
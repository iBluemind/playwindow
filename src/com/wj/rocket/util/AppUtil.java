package com.wj.rocket.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class AppUtil {

	private static Toast logToast = null;
	private static ProgressDialog waitDlg = null;
	
	public static void showToast(final Activity activity, final String msg) {
		activity.runOnUiThread(new Runnable(){
		   public void run()
		   {
			  if (logToast != null) {
				   logToast.cancel();
				   logToast = null;
			  }
			   logToast = Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_SHORT);
			   logToast.show();
		   }
		   
	   }); 
	}
	
	public static void showWaitDialog(final Activity activity, final String title, final String message) {
		
		activity.runOnUiThread(new Runnable(){
		   public void run()
		   {
			  if (waitDlg != null) {
				  waitDlg.dismiss();;
				  waitDlg = null;
			  }
			  waitDlg = ProgressDialog.show(activity, title, message, true, false);
		   }
		 }); 
	}
	public static boolean isShowingWaitDlg() {
		if (waitDlg != null) {
			return waitDlg.isShowing();
		}
		return false;

	}
	public static void hideWaitDialog(final Activity activity) {
		if (waitDlg == null) {
			return;
		}
		activity.runOnUiThread(new Runnable(){
		   public void run()
		   {
			   if(waitDlg.isShowing()) {
				   waitDlg.dismiss();
			   }
			   waitDlg = null;
		   }
		 });
	}
	
	// PlatRTC Service Wrapper에 요청할 서비스 고유 아이디를 생성
    // 사용자 아이디 + 인의의 문자열(5자) 결합 하여 생성
    public static String makeRequestPlayRTCServiceId(String userId)
	{
		String randId = "";
		String possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for( int i=0; i < 5; i++ )
		{
			double randomVal = Math.random();
			int idx = (int)Math.floor(randomVal * possible.length());
			randId += possible.charAt(idx);
		}
		return userId + randId;
	}
    
    // Device UUID 생성 
    public static String getDeviceUUID(Context context) {
		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE); 
		String tmDevice, tmSerial, androidId; 
	    tmDevice = "" + tm.getDeviceId(); 
	    tmSerial = "" + tm.getSimSerialNumber(); 
	    androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID); 
	 
	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode()); 
	    return deviceUuid.toString();
	}
    
    public static boolean isWifiConnected(Context context)
	{
		ConnectivityManager cm = getConnectivityManager(context);
		NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return (ni==null)?false:ni.isConnected();
	}
	
	public static boolean isMobileConnected(Context context)
	{
		ConnectivityManager cm = getConnectivityManager(context);
		NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isConnected = (ni == null)?false:ni.isConnected();
		return isConnected;
	}
	
	public static String getWifiSSID(Context context) {
		WifiManager wifimanager;
		wifimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

		WifiInfo info = wifimanager.getConnectionInfo();

		String ssid = info.getSSID();
		if(ssid != null)ssid = ssid.replaceAll("\"", "");
		//int speed = info.getLinkSpeed();
		//tvWifi.setText("SSID : " + ssid + ", LinkSpeed : " + speed );
		return ssid;
	}
	private static ConnectivityManager getConnectivityManager(Context context) {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm;
	}
    public static String getElasedTimeFormat(long millis) {
		String _text = null;
		if(millis > 0) {
			long second = (millis / 1000) % 60;
			long minute = (millis / (1000 * 60)) % 60;
			long hour = (millis / (1000 * 60 * 60)) % 24;
			_text = String.format("%02d:%02d:%02d", hour, minute, second);
		}
		else {
			_text = "--:--:--";
		}
		return _text;
	}
	
    public static String getDiscountTimeText(long millis) {
		String _text = null;
		if(millis > 0) {
			long second = (millis / 1000) % 60;
			long minute = (millis / (1000 * 60)) % 60;
			//long hour = (millis / (1000 * 60 * 60)) % 24;
			_text = String.format("-%02d:%02d", minute, second);
		}
		else {
			_text = "00:00";
		}
		return _text;
	}
	
    public static String getDateTime(Date date, String format)
    {
	 	if(format ==null || format.length()==0)format = "yyyyMMdd";
    	SimpleDateFormat formatter = new SimpleDateFormat ( format, Locale.KOREA );
    	String dTime = formatter.format ( date );
    	return dTime;
    }
    
    public static int setMasterVolume(Context context, int playVol) {
    		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    	int currVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	    	int maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
	    	if(playVol < 0 || playVol > maxVol) return currVol;
	    	audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, playVol, AudioManager.FLAG_PLAY_SOUND);
	    	
	    	return currVol;
    }
    
    public static int setMasterVolumeMax(Context context) {
    	
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    	int currVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	    	
	    	Toast.makeText(context, "before: " + String.valueOf(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)), Toast.LENGTH_SHORT).show();
	    	
	    	int maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
	    	audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVol, AudioManager.FLAG_PLAY_SOUND);
	    	
     	Toast.makeText(context, "after: " + String.valueOf(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)), Toast.LENGTH_SHORT).show();
	    	
	    	return currVol;
	}
    
    public static int getMasterVolume(Context context) {
    		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    	int currVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	 
	    	return currVol;
    }

//    public static int setSystemVolume(Context context, int playVol) {
//    		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//    	
//	    	int currVol = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
//	    	int maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
//	    	if(playVol < 0 || playVol > maxVol) return currVol;
//	    	audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, playVol, AudioManager.FLAG_PLAY_SOUND);
//	    	
//	    	return currVol;
//    }

}

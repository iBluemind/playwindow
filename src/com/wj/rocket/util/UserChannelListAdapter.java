package com.wj.rocket.util;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhaarman.listviewanimations.ArrayAdapter;
import com.wj.rocket.R;
import com.wj.rocket.model.Channel;

public class UserChannelListAdapter extends ArrayAdapter<Channel> {
	
	private LayoutInflater layoutInflater;
	private List<Channel> userChannels;
	
	public UserChannelListAdapter(Context context, List<Channel> userChannels) {
		this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.userChannels = userChannels;
		
	}

	@Override
	public int getCount() {
		return userChannels.size();
	}

	@Override
	public Channel getItem(int position) {
		return userChannels.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null)
			view = layoutInflater.inflate(R.layout.item_user_channel_list, null, false);
		
		TextView tvName = (TextView) view.findViewById(R.id.tvName);
		TextView tvDate = (TextView) view.findViewById(R.id.tvDate);
		
		final Channel userChannel = userChannels.get(position);
		
		tvName.setText(userChannel.getName());
		tvDate.setText(userChannel.getCreatedAt());
		
		return view;
	}
	

}

package com.wj.rocket.blackbox;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Toast;

public class GyroSensorMotion implements SensorEventListener {
	
	private SensorManager sensorManager;
	private Sensor gyroSensor;
	
	private Context context;
	private OnGyroCallback gyroCallback;
	
	float oldY = 0.0f;
	
	public GyroSensorMotion(Context context) {
		this.context = context;
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		
		gyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	}
	
	public void startGyroMotion() {
		sensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_FASTEST);
	}
	
	public void stoptGyroMotion() {
			sensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		float y = event.values[1] * 100;
		
		Log.i("GyroSensorMotion", "Car Moviinv!!! X :" + y + ", " + "OldX : " + oldY);
		if ( Math.abs(oldY - y) > 50) {
			if (gyroCallback != null) {
				gyroCallback.OnReceivedCrashed();
			}
		}
		
		oldY = y;
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}
	
	public void setCallback(OnGyroCallback callback) {
		this.gyroCallback = callback;
	}
}

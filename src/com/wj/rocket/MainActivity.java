package com.wj.rocket;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.skt.baas.api.Baas;
import com.skt.baas.api.BaasFacebookUtils;
import com.skt.baas.api.BaasInstallation;
import com.skt.baas.api.BaasUser;
import com.skt.baas.auth.FacebookAuthenticationProvider;
import com.skt.baas.callback.BaasLoginCallback;
import com.skt.baas.callback.BaasSaveCallback;
import com.skt.baas.exception.BaasException;
import com.wj.rocket.activity.LoginActivity;
import com.wj.rocket.base.BaseActivity;
import com.wj.rocket.base.BaseFragment;
import com.wj.rocket.util.AppUtil;
import com.wj.rocket.util.LogUtil;
import com.wj.rocket.util.OnLoadListener;
import com.wj.rocket.util.OnLoginListener;
import com.wj.rocket.util.SectionsPagerAdapter;

public class MainActivity extends BaseActivity implements OnLoginListener {

	public static final int INDEX_FRAGMENT_CHANNEL = 0;
	public static final int INDEX_FRAGMENT_SETTING = 1;

	private SectionsPagerAdapter sectionsPagerAdapter;
	private ViewPager viewPager;
	private PagerSlidingTabStrip pagerSlidingTabStrip;
	private FragmentManager fragmentManager;
	public int lastPageFragment = 0;
	private BroadcastReceiver refreshChannelListReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (DEBUG)
			printHashKey();

		if (initBaas()) {
			initialize();

		} else {
			onError();
			finish();
		}

	}
	
	private void printHashKey() {
		try {
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.wj.rocket", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("hashKey", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {
	    	e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
	    	e.printStackTrace();
	    }
	}

	private boolean initBaas() {
		Baas.debugMode(DEBUG);

		if (AppUtil.isWifiConnected(this) || AppUtil.isMobileConnected(this)) {
			Baas.init(MainActivity.this.getApplicationContext(), 1,
					API_KEY_BAAS);
			BaasFacebookUtils.initialize(getString(R.string.facebook_app_id));
			
			return true;
		}
		return false;
	}

	@Override
	protected void onResume() {
		super.onResume();

		// 자동 로그인을 위한 부분, 이전에 로그인을 했을 경우 BaasUser.getCurrentUser()가 존재
		if (BaasUser.getCurrentUser() == null) {
			Intent intent = new Intent(MainActivity.this, LoginActivity.class);
			startActivityForResult(intent, REQUEST_CODE_ACTIVITY_LOGIN);
		} else {

			if (BaasFacebookUtils.isLinked(BaasUser.getCurrentUser())) {
				if (BaasFacebookUtils.getSession() == null) {
					lockUI();
					
					BaasFacebookUtils.logIn(this, new BaasLoginCallback() {

						@Override
						public void onSuccess(BaasUser user, BaasException e) {
							unLockUI();
							
							if (e != null) {
								// 실패
								LogUtil.e("FacebookLogin", e.getMessage());
							}
						}
					});
				}
			}
			
			refreshChannelListReceiver = new BroadcastReceiver() {
			    @Override
			    public void onReceive(Context context, Intent intent) {
			    	ChannelFragment channelFragment = (ChannelFragment) getFragment(INDEX_FRAGMENT_CHANNEL);
			    	channelFragment.getChannelList();
			    	
			    }
			};
			registerReceiver(refreshChannelListReceiver, new IntentFilter(INTENT_REFRESH_CHANNEL_LIST));
		}

	}      
	
	@Override
	protected void onPause() {
		if (refreshChannelListReceiver != null)
			unregisterReceiver(refreshChannelListReceiver);
		super.onPause();
	}
	
	@Override
	public void lockUI() {
		List<BaseFragment> fragments = getFragmentList();
		
		if (fragments != null) {
			for (Fragment fragment : fragments) {
				if (fragment instanceof OnLoadListener) {
					((OnLoadListener) fragment).lockUI();
				}
			}
		}
		
		super.lockUI();
	}
	
	@Override
	public void unLockUI() {
		super.unLockUI();
		
		List<BaseFragment> fragments = getFragmentList();
		
		if (fragments != null) {
			for (Fragment fragment : fragments) {
				if (fragment instanceof OnLoadListener) {
					((OnLoadListener) fragment).unLockUI();
				}
			}
		}
	}
	
	@Override
	public void onLoggedIn() {
		List<BaseFragment> fragments = getFragmentList();
		
		if (fragments != null) {
			for (BaseFragment fragment : fragments) {
				if (fragment instanceof OnLoginListener) {
					((OnLoginListener) fragment).onLoggedIn();
				}
			}
		}
	}
	
	@Override
	public void onLoggedOut() {
		List<BaseFragment> fragments = getFragmentList();
		
		if (fragments != null) {
			for (Fragment fragment : fragments) {
				if (fragment instanceof OnLoginListener) {
					((OnLoginListener) fragment).onLoggedOut();
				}
			}
		}

	}
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_ACTIVITY_LOGIN) {
			if (resultCode != RESPONSE_CODE_LOGIN_SUCCESS) {
				finish();
			} else {
				BaasInstallation.getCurrentInstallation().serverSaveInBackground(
					new BaasSaveCallback() {
	
						@Override
						public void onSuccess(BaasException e) {
							if (e != null) {
								// 실패
							} else {
								// 성공
								BaasInstallation.isUseBaasPush(true);
								GCMIntentService.checkAndGCMRegister(getApplicationContext());
								
							}
						}
				});
			}
		}
	}

	private void initialize() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
		
		fragmentManager = getSupportFragmentManager();
		sectionsPagerAdapter = new SectionsPagerAdapter(fragmentManager,
				getFragmentList());

		viewPager = (ViewPager) findViewById(R.id.viewPager);
		viewPager.setAdapter(sectionsPagerAdapter);

		pagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.pagerSlidingTabStrip);
		pagerSlidingTabStrip.setSmoothScrollingEnabled(true);
		pagerSlidingTabStrip
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

					@Override
					public void onPageScrolled(int position,
							float positionOffset, int positionOffsetPixels) {
					}

					@Override
					public void onPageSelected(int position) {
						lastPageFragment = position;
					}

					@Override
					public void onPageScrollStateChanged(int state) {
					}
				});

		viewPager.setOffscreenPageLimit(3);
		pagerSlidingTabStrip.setViewPager(viewPager);

	}
	
	public List<BaseFragment> getFragmentList() {
        List<BaseFragment> fragmentList = new ArrayList<BaseFragment>();

        fragmentList.add(ChannelFragment.newInstance());
        fragmentList.add(SettingFragment.newInstance());

        return fragmentList;
    }

    public BaseFragment getFragment(int index) {
    	BaseFragment newFragment = null;

        try {
            newFragment = (BaseFragment) sectionsPagerAdapter.getItem(index);
        } catch (Exception e) {
            sectionsPagerAdapter.setFragmentList(getFragmentList());
            newFragment = getFragment(index);
        }

        return newFragment;
    }
    
    @Override
    public void onBackPressed() {
    	if (viewPager != null) {
    		if (viewPager.getCurrentItem() == INDEX_FRAGMENT_CHANNEL) {
    			super.onBackPressed();
    		} else {
    			viewPager.setCurrentItem(INDEX_FRAGMENT_CHANNEL);
    		}
    	}
    }
    
}

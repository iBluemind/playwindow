package com.wj.rocket.model;

public class User {
	
	private String objectId;
	private String username;
	private String password;
	protected String installationId;
	private String gcmDeviceId;
	
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getInstallationId() {
		return installationId;
	}
	public void setInstallationId(String installationId) {
		this.installationId = installationId;
	}
	public String getGcmDeviceId() {
		return gcmDeviceId;
	}
	public void setGcmDeviceId(String gcmDeviceId) {
		this.gcmDeviceId = gcmDeviceId;
	}
	
	

}

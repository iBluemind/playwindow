package com.wj.rocket.model;

public class Setting {

    public static final int TYPE_SETTING_ENTRY_COMMON = 0;
    public static final int TYPE_SETTING_ENTRY_TOGGLE = 1;
    public static final int TYPE_SETTING_ENTRY_HEADER = 2;

    private int type;
    private int id;
    private String title;
    private String desc;
    private String value;

    public Setting(int type, String title) {
        this.type = type;
        this.title = title;
    }

    public Setting(int type, int id, String title) {
        this.id = id;
        this.type = type;
        this.title = title;
    }


    public Setting(int type, int id, String title, String desc, String value) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.desc = desc;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
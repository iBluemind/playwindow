package com.wj.rocket.sound;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.wj.rocket.util.AppUtil;

public class SoundAnalyzer implements TextToSpeech.OnInitListener{
	
	private TextToSpeech tts;
	
	private Context context;
	
	private String[] messages = {
		"저녁 먹었어?",
		"별일 없지?",
		"오늘 술 먹었어?",
		"집에 전화는 했어?"
	};
	
	private Intent intent;
	private SpeechRecognizer speechRecognizer;
	
	private OnSpeechCallback speechCallback;
	
	public SoundAnalyzer(Context context) {
		this.context = context;
		intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getPackageName());
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR");
		
		speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
		speechRecognizer.setRecognitionListener(speechListener);
		
	}

	private RecognitionListener speechListener = new RecognitionListener() {
		
		@Override
		public void onRmsChanged(float rmsdB) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onResults(Bundle results) {

			String key = "";
		    key = SpeechRecognizer.RESULTS_RECOGNITION;
		    ArrayList<String> mResult = results.getStringArrayList(key);
		    String[] rs = new String[mResult.size()];
		    mResult.toArray(rs);
		    
		    for ( String speech : rs ) {
		    		Log.e("SoundAnalyzer", speech);
			    	if ( speech.equals("응") || speech.equals(("아니")) ) {
			    		if (speechCallback != null) {
			    			speechCallback.onReceivedResponse();
			    		} else {
			    			throw new IllegalStateException("Callback must be not null!");
			    			
			    		}
			    	}
		    }
		}
		
		@Override
		public void onReadyForSpeech(Bundle params) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onPartialResults(Bundle partialResults) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onEvent(int eventType, Bundle params) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onError(int error) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onEndOfSpeech() {
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
			int volume = sharedPreferences.getInt("old_volume", 1);
			AppUtil.setMasterVolume(context, volume);
			
		}
		
		@Override
		public void onBufferReceived(byte[] buffer) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onBeginningOfSpeech() {
			Log.e("SoundAnalyzer", "Beginning Speech");
			
		}
	};
	public void startTTS() {
		tts = new TextToSpeech(context, this);
	}
	
	@Override
	public void onInit(int status) {
		
		if ( status == TextToSpeech.SUCCESS ) {
			int result = tts.setLanguage(Locale.KOREA);
			
			if ( result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED )
				Log.e("TTSManager", "Langulage is not available");
			else {
				speak();
			}
		}
	}
	
	private void speak() {
		Random random = new Random();
		String message = messages[random.nextInt(messages.length)];
		
		AppUtil.setMasterVolumeMax(context);
		tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
		
		speechRecognizer.startListening(intent);
		
	}

	public void stopTTS() {
		if ( tts != null ) {
			tts.stop();
			tts.shutdown();
		}
	}
	
	public void setSpeechCallback(OnSpeechCallback callback) {
		this.speechCallback = callback;
	}
	
}

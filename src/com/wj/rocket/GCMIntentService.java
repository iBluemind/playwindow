package com.wj.rocket;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.google.gson.Gson;
import com.skt.baas.api.BaasInstallation;
import com.skt.baas.api.BaasUser;
import com.wj.rocket.activity.CameraActivity;
import com.wj.rocket.model.Channel;
import com.wj.rocket.network.BaasDeviceInstallation;
import com.wj.rocket.network.BaasServiceHelper;
import com.wj.rocket.util.Constants;
import com.wj.rocket.util.LogUtil;

public class GCMIntentService extends GCMBaseIntentService implements Constants
{
	public static final String LOG_TAG = GCMIntentService.class.getSimpleName();
	
	private void alert(final Context context, Channel channelData, String title, String message, long recvTime) {
		String objectId = channelData.getObjectId();
		String channelId = channelData.getChannelId();
		String channelName = channelData.getName();
		
		PendingIntent pendingIntent = null;
		
		Intent notiIntent = null; 
		
		notiIntent = new Intent(context, CameraActivity.class);
		notiIntent.putExtra("objectId", objectId);
		notiIntent.putExtra("channelId", channelId);
		notiIntent.putExtra("channelName", channelName);
		notiIntent.putExtra("callingType", CALL_TYPE_RESPONSE);
		notiIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP  | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		pendingIntent = PendingIntent.getActivity(context, 0, notiIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder =  new NotificationCompat.Builder(context);
		builder.setContentIntent(pendingIntent);
		builder.setSmallIcon(R.drawable.ic_launcher);
		builder.setContentTitle(title);
		builder.setContentText(message);
		builder.setAutoCancel(true); //// 알림바에서 자동 삭제
		builder.setVibrate(new long[]{0,500,500,500}); // 진동 2번 
		builder.setLights(0xff00ff00, 300, 100); // LED 작동 
		
		Notification notification = builder.build();
		notification.defaults |= Notification.DEFAULT_SOUND;
		notificationManager.notify(0, notification);
	    
   }
	
   public GCMIntentService(){
       super(NUMBER_GCM_PROJECT);
   }
   
   @Override
   protected void onError(Context context, String errorId) {
	   LogUtil.e(LOG_TAG, "GCM Error errorId[%s]", errorId);
   }
    
   @Override
   protected void onMessage(Context context, Intent intent) {
	   long recvTime = System.currentTimeMillis();
	   
	   String action = intent.getAction();
	   // GCM메세지를 수신받았을 때 발생되는 action인지 체크한다.
	   if ("com.google.android.c2dm.intent.RECEIVE".equals(action)) 
	   {
	 	   	Bundle bundle = intent.getExtras();
	 	   	String type = bundle.getString("title");
	 	   	String alertMsg = bundle.getString("alert");
	 	    
	 	   	LogUtil.e(LOG_TAG, "GCM Message[%s]", alertMsg);
	 	    
	 	   	try {
		 	   	Gson gson = new Gson();
		 	    Channel channelData = gson.fromJson(alertMsg, Channel.class);
		 	    
		 	    if (type.equals(TYPE_PUSH_DETECT_INVASION)) {
		 	    		alert(context, channelData, context.getString(R.string.push_detect_invasion_title),
		 	    				context.getString(R.string.push_detect_invasion_message), recvTime);
		 	    } else if (type.equals(TYPE_PUSH_DETECT_CRASH)) {
		 	    		alert(context, channelData, context.getString(R.string.push_detect_crash_title),
	 	    				context.getString(R.string.push_detect_crash_message), recvTime);
		 	    } else if (type.equals(TYPE_PUSH_REFRESH_CHANNELS)) {
			 	    	Intent intentRefreshChannels = new Intent(INTENT_REFRESH_CHANNEL_LIST);
			 	    	context.sendBroadcast(intentRefreshChannels);
		 	    }
	 	   		
	 	   	} catch (Exception e) {
	 	   		if (DEBUG)
	 	   			e.printStackTrace();
	 	   	}
	 	    
	    }
   }

   @Override
   protected void onRegistered(Context context, String registrationId) {
	   LogUtil.e(LOG_TAG, "GCM 키를 등록했습니다.[%s]", registrationId);
	   BaasServiceHelper.newInstance().deviceUpdateInstallation(
				BaasInstallation.getCurrentInstallation().getObjectId(),
				registrationId,
				BaasUser.getCurrentUser().getObjectId(), 
				new Callback<BaasDeviceInstallation>() {
					
					@Override
					public void success(BaasDeviceInstallation baasInstallation, Response response) {
						
					}

					@Override
					public void failure(RetrofitError e) {
						if (DEBUG)
							e.printStackTrace();
					}
					
		});
   }
    
   @Override
   protected void onUnregistered(Context context, String registrationId) {
	   LogUtil.e(LOG_TAG, "GCM 키가 제거되었습니다.[%s]", registrationId);
   }
   
// GCM Device 아이디를 조사하고 없으면 GCM 등록 프로세스 시작 
   // GCMRegistrationListener : GCM 등록/해지 이벤트를 전달 받는 리스너 
   public static String checkAndGCMRegister(Context context) {

   	String regId = getGCMRegistrationId(context);
       if (TextUtils.isEmpty(regId)) {
       	GCMRegistrar.register(context, NUMBER_GCM_PROJECT);
       }
       else {
       	return regId;
       }
       return null;
   }
   // GCM Device 아이디를 조사하고 존재하면 GCM 등록 해제 프로세스 시작 
   public static void checkAndGCMUnRegister(Context context) {
   	LogUtil.d("GCM", "checkAndUnregistServices");
   	String regId = getGCMRegistrationId(context);
   	LogUtil.d("GCM", "regId="+regId);
       if (TextUtils.isEmpty(regId) == false) {
       	LogUtil.d("GCM", "call GCMRegistrar.unregister");
       	GCMRegistrar.unregister(context);
       }
   }
	
   
   // GCM registration id를 조회하여 반환한다.
   public static String getGCMRegistrationId(Context context)
   {
   	LogUtil.d("GCM", "getGCMRegistrationId()");
   	LogUtil.d("GCM", "GCMRegistrar.checkDevice");
   	GCMRegistrar.checkDevice(context);
   	LogUtil.d("GCM", "GCMRegistrar.checkManifest");
   	GCMRegistrar.checkManifest(context);
   	// 등록되어 있는 아이디가 있다면 반환 
   	final String registrationId = GCMRegistrar.getRegistrationId(context);
		if (TextUtils.isEmpty(registrationId))
		{
			LogUtil.i("GCMRegistrar", "GCMRegistrar Registration not found.");
			return "";
		}
		LogUtil.d("GCM", "getRegistrationId :: "+registrationId);
		return registrationId;
   }
}

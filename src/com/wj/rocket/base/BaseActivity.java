package com.wj.rocket.base;

import android.app.ActionBar;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.wj.rocket.BuildConfig;
import com.wj.rocket.R;
import com.wj.rocket.util.Constants;
import com.wj.rocket.util.OnLoadListener;

public class BaseActivity extends ActionBarActivity implements OnLoadListener, Constants {
	
	public ActionBar actionBar;
	public SharedPreferences sharedPreferences;
	protected Toast mToast;
	
	private LoadingDialog mLockUI;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		mLockUI = new LoadingDialog(this);
		
	}
	
	/**
	 * 액션바를 설정하는 메서드로서, 어플리케이션 액션바 테마를 설정하고 제목을 지정한다.
	 * 
	 * @param title 액션바에 표시할 화면의 제목을 받는다.
	 */
	public void setActionBar(String title) {
		actionBar = getActionBar();
		
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().
				getColor(android.R.color.white)));
		
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		
//		actionBar.setIcon(R.drawable.img_ic_menu);
		actionBar.setTitle(title);
		
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
	}
	
	/**
	 * 액션바에 ProgressBar를 표시할 수 있도록 셋팅한다.
	 */
	public void setActionBarProgressBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
			setProgressBarIndeterminate(true);
		}
	}
	
	/**
	 * 액션바를 숨기도록 셋팅한다.
	 * 
	 */
	public void setActionBarHide() {
		getActionBar().hide();
		
	}
	  
	
	@Override
	protected void onPause() {
		
		// 현재 Activity에 의존적인 Toast를 제거한다.
		if (mToast != null)
			mToast.cancel();
		
		super.onPause();
		
	}
	
	@Override
	protected void onDestroy() {
		unLockUI();
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * LoadingDialog를 띄워 로딩 중임을 나타내어 사용자가 UI를 사용할 수 없도록 한다.
	 */
	@Override
	public void lockUI() {
		if (mLockUI != null && !isFinishing())
			mLockUI.show();
	}

	/**
	 * 로딩이 완료되어 LoadingDialog를 제거하고 전역 폰트를 설정한다.
	 */
	@Override
	public void unLockUI() {
		if (mLockUI != null)
			mLockUI.hide();
		
	}

	public void onError(Exception error) {
		if (DEBUG) {
			error.printStackTrace();
		}
		
		onError();
	}
	
	/**
	 * Error 발생 시 분기되는 메서드
	 */
	public void onError() {
		unLockUI();
		showToast(R.string.err_network, Toast.LENGTH_LONG, false);
	}
	
	/**
	 * Toast를 쉽게 표시해주는 메서드로서, 참조 Context로는 ApplicationContext를 사용한다. 
	 * 삼성 단말기에서 삼성 테마를 사용하기 위함이다.
	 * 
	 * @param message Toast에 표시할 내용
	 * @param length Toast가 표시되는 시간. Toast.LENGTH_SHORT, Toast.LENGTH_LONG
	 * @param isAttachToActivity	현재 Activity가 종료되면 Toast도 제거할지를 결정한다
	 */
	public void showToast(String message, int length, boolean isAttachToActivity) {
		if (mToast != null)
			mToast.cancel();
		
		if (isAttachToActivity) {
			mToast = Toast.makeText(getApplicationContext(), message, length);
			mToast.show();
			
		} else {
			Toast.makeText(getApplicationContext(), message, length).show();
			
		}
	}
	
	public void showToast(int messageStringId, int length, boolean isAttachToActivity) {
		showToast(getString(messageStringId), length, isAttachToActivity);
	}

}

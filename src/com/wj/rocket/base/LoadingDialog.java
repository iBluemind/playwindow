package com.wj.rocket.base;

import com.wj.rocket.R;
import com.wj.rocket.R.style;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;

public class LoadingDialog {
	
	private Dialog mDialog;

	public LoadingDialog(final BaseActivity activity) {
		mDialog = new Dialog(activity, R.style.TransparentDialog);
		ProgressBar pb = new ProgressBar(activity);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		
		mDialog.addContentView(pb, params);
		mDialog.setCancelable(true);
		mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				hide();
				activity.onBackPressed();

			}
		});
	}

	public void show() {
		if (!mDialog.isShowing())
			mDialog.show();
	}

	public void hide() {
		if (mDialog.isShowing())
			mDialog.dismiss();
	}

}

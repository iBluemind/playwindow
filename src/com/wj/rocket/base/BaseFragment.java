package com.wj.rocket.base;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wj.rocket.MainActivity;
import com.wj.rocket.util.Constants;
import com.wj.rocket.util.OnLoadListener;

public class BaseFragment extends Fragment implements Constants, OnLoadListener  {

    private String name;
    private String title;

    protected BaseActivity hostActivity;
    protected SharedPreferences sharedPreferences;
    
    @Override
    public void onAttach(Activity activity) {
	    	super.onAttach(activity);
	    	if (activity instanceof BaseActivity)
	    		hostActivity = (BaseActivity) activity; 
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        hostActivity = (BaseActivity) getActivity();
    }

    @Override
    public void lockUI() {
	    	if (hostActivity != null)
	    		hostActivity.lockUI();
    }

    @Override
    public void unLockUI() {
	    	if (hostActivity != null)
	    		hostActivity.unLockUI();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void showToast(String message, int length, boolean isAttachToActivity) {
        hostActivity.showToast(message, length, isAttachToActivity);
    }
    
    public void showToast(int message, int length, boolean isAttachToActivity) {
        hostActivity.showToast(message, length, isAttachToActivity);
    }

    public void onError(Exception error) {
        hostActivity.onError(error);
    }

    /**
     * Error 발생 시 분기되는 메서드
     */
    public void onError() {
        hostActivity.onError();
    }
}

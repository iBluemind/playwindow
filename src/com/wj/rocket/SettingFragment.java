package com.wj.rocket;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.skt.baas.api.BaasFacebookUtils;
import com.skt.baas.api.BaasUser;
import com.wj.rocket.activity.AboutActivity;
import com.wj.rocket.activity.LoginActivity;
import com.wj.rocket.base.BaseFragment;
import com.wj.rocket.model.Setting;
import com.wj.rocket.util.OnLoginListener;
import com.wj.rocket.util.SettingListAdapter;

public class SettingFragment extends BaseFragment implements OnLoginListener,
		OnItemClickListener {

	private static final int ID_SETTING_ENABLE_FACE_DETECT = 0;
	private static final int ID_SETTING_ENABLE_VOICE_RECOGNITION = 1;
	private static final int ID_SETTING_ENABLE_BLACK_BOX = 2;
	private static final int ID_SETTING_ENABLE_LOGOUT = 3;
	private static final int ID_SETTING_ENABLE_ABOUT = 4;

	private List<Setting> settings;
	private ListView lvSettings;
	private SettingListAdapter adapter;

	public static SettingFragment newInstance() {
		SettingFragment settingFragment = new SettingFragment();
		settingFragment.setTitle("설정");
		
		return settingFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_setting, container, false);

		lvSettings = (ListView) v.findViewById(R.id.lvSettings);
		lvSettings.setOnItemClickListener(this);
		
		refreshListView();
		
		return v;
	}
	
	private void refreshListView() {
		if (settings == null) {
			settings = new ArrayList<Setting>();
		} else {
			settings.clear();
		}
		
		settings.add(new Setting(Setting.TYPE_SETTING_ENTRY_TOGGLE,
				ID_SETTING_ENABLE_FACE_DETECT, "얼굴인식 활성화", "얼굴인식 기능을 켜고 끕니다.",
				Boolean.toString(sharedPreferences.getBoolean(SETTING_ENABLE_DETECT_FACE, false))));
		settings.add(new Setting(Setting.TYPE_SETTING_ENTRY_TOGGLE,
				ID_SETTING_ENABLE_VOICE_RECOGNITION, "음성인식 활성화",
				"음성인식 기능을 켜고 끕니다.", Boolean.toString(sharedPreferences.getBoolean(SETTING_ENABLE_VOICE_RECOGNITION, false))));
		settings.add(new Setting(Setting.TYPE_SETTING_ENTRY_TOGGLE,
				ID_SETTING_ENABLE_BLACK_BOX, "블랙박스 활성화", 
				"블랙박스 기능을 켜고 끕니다.", Boolean.toString(sharedPreferences.getBoolean(SETTING_ENABLE_BLACK_BOX, false))));
		settings.add(new Setting(Setting.TYPE_SETTING_ENTRY_COMMON,
				ID_SETTING_ENABLE_LOGOUT, "로그아웃", "PlayWindow의 계정을 로그아웃합니다.", ""));
		settings.add(new Setting(Setting.TYPE_SETTING_ENTRY_COMMON,
				ID_SETTING_ENABLE_ABOUT, "어플리케이션 정보", "PlayWindow에 대해 소개합니다.", ""));

		if (adapter == null) {
			adapter = new SettingListAdapter(getActivity(), settings);	
			lvSettings.setAdapter(adapter);
		} else {
			adapter.notifyDataSetChanged();
		}
		
	}

	@Override
	public void onLoggedIn() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoggedOut() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> adpaterView, View view, int i, long l) {
		Setting selectedItem = (Setting) adpaterView.getItemAtPosition(i);
		int id = selectedItem.getId();
		SharedPreferences.Editor preferenceEditor = null;

		switch (id) {
		case ID_SETTING_ENABLE_LOGOUT:
			new AlertDialog.Builder(getActivity())
					.setMessage("로그아웃하시겠습니까?")
					.setCancelable(false)
					.setPositiveButton(android.R.string.yes,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									BaasUser.logOut();
									
									Intent intent = new Intent(
											getActivity(),
											LoginActivity.class);
									startActivityForResult(intent, REQUEST_CODE_ACTIVITY_LOGIN);
								}
							}).setNegativeButton(android.R.string.no, null)
					.show();
			break;
		case ID_SETTING_ENABLE_FACE_DETECT:
			boolean currentValue = Boolean.parseBoolean(selectedItem.getValue());
			boolean newValue = !currentValue;
			
			preferenceEditor = sharedPreferences.edit();
			preferenceEditor.putBoolean(SETTING_ENABLE_DETECT_FACE, newValue);
			preferenceEditor.commit();
			refreshListView();
			
			if (newValue) {
				try {
					captureSrcreenTest();	// 단순히 수퍼유저 권한 물음창을 띄우기 위함임.
				} catch (Exception e) {
					preferenceEditor = sharedPreferences.edit();
					preferenceEditor.putBoolean(SETTING_ENABLE_DETECT_FACE, false);
					preferenceEditor.commit();
					refreshListView();
					
					showToast(R.string.err_not_superuser, Toast.LENGTH_SHORT, true);
				}
				
			}
			break;
		case ID_SETTING_ENABLE_ABOUT:
			startActivity(new Intent(getActivity(), AboutActivity.class));
			break;
			
		case ID_SETTING_ENABLE_VOICE_RECOGNITION:
			preferenceEditor = sharedPreferences.edit();
			preferenceEditor.putBoolean(SETTING_ENABLE_VOICE_RECOGNITION, !Boolean.parseBoolean(selectedItem.getValue()));
			preferenceEditor.commit();
			refreshListView();
			break;
		case ID_SETTING_ENABLE_BLACK_BOX:
			preferenceEditor = sharedPreferences.edit();
			preferenceEditor.putBoolean(SETTING_ENABLE_BLACK_BOX, !Boolean.parseBoolean(selectedItem.getValue()));
			preferenceEditor.commit();
			refreshListView();
			break;
			
		}
	}
	
	private void captureSrcreenTest() throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append(getActivity().getFilesDir());
		sb.append("/");
		sb.append(Math.abs(UUID.randomUUID().hashCode()));
		sb.append(".png");
		String filePath = sb.toString();
		
		DataOutputStream os = null;
		
		try {
			Process sh = Runtime.getRuntime().exec("su", null, null);
			os = new DataOutputStream(sh.getOutputStream());
			
			os.writeBytes("/system/bin/screencap -p " + filePath + "\n");
			os.writeBytes("/system/bin/chmod 644 " + filePath + "\n");
			os.writeBytes("exit\n");
			os.flush();
			os.close();
			sh.waitFor();
			
			(new File(filePath)).delete();
			
		} catch (Exception e) {
			if (DEBUG)
				e.printStackTrace();
			throw e;
		} finally {
			if (os != null)
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_ACTIVITY_LOGIN) {
			if (resultCode != RESPONSE_CODE_LOGIN_SUCCESS) {
				hostActivity.finish();
			}
		}
	}
}
